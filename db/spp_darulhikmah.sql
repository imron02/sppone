--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: array_accum(anyelement); Type: AGGREGATE; Schema: public; Owner: imron
--

CREATE AGGREGATE array_accum(anyelement) (
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


ALTER AGGREGATE public.array_accum(anyelement) OWNER TO imron;

--
-- Name: array_agg(anyelement); Type: AGGREGATE; Schema: public; Owner: imron
--

CREATE AGGREGATE array_agg(anyelement) (
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


ALTER AGGREGATE public.array_agg(anyelement) OWNER TO imron;

--
-- Name: auto_increment_admin; Type: SEQUENCE; Schema: public; Owner: imron
--

CREATE SEQUENCE auto_increment_admin
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_increment_admin OWNER TO imron;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE admin (
    username character varying(50) NOT NULL,
    password character varying(150) NOT NULL,
    nama character varying(50),
    email character varying(100),
    id integer DEFAULT nextval('auto_increment_admin'::regclass) NOT NULL
);


ALTER TABLE public.admin OWNER TO imron;

--
-- Name: admin_sequence; Type: SEQUENCE; Schema: public; Owner: imron
--

CREATE SEQUENCE admin_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_sequence OWNER TO imron;

--
-- Name: auto_increment_transaksi_spp; Type: SEQUENCE; Schema: public; Owner: imron
--

CREATE SEQUENCE auto_increment_transaksi_spp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_increment_transaksi_spp OWNER TO imron;

--
-- Name: guru; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE guru (
    no_induk_guru integer NOT NULL,
    nama character varying(50) NOT NULL,
    tlp character varying(14),
    ttl character varying,
    jk character varying(1),
    ijazah character varying,
    jabatan character varying(30),
    mengajar character varying(5),
    bidang_studi character varying(100)
);


ALTER TABLE public.guru OWNER TO imron;

--
-- Name: sequence; Type: SEQUENCE; Schema: public; Owner: imron
--

CREATE SEQUENCE sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence OWNER TO imron;

--
-- Name: siswa; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE siswa (
    no_induk integer NOT NULL,
    nama character varying(50) NOT NULL,
    kelas character varying(6) NOT NULL,
    keterangan character varying(7)
);


ALTER TABLE public.siswa OWNER TO imron;

--
-- Name: spp; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE spp (
    kelas character varying(5) NOT NULL,
    spp numeric(12,0),
    bangunan integer,
    lain_lain integer
);


ALTER TABLE public.spp OWNER TO imron;

--
-- Name: transaksi; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE transaksi (
    no_induk integer,
    nama character varying(50),
    kelas character varying(6),
    spp numeric(12,0),
    dibayar numeric,
    tunggakan numeric,
    bulan character varying(255)
);


ALTER TABLE public.transaksi OWNER TO imron;

--
-- Name: transaksi_spp; Type: TABLE; Schema: public; Owner: imron; Tablespace: 
--

CREATE TABLE transaksi_spp (
    no_transaksi integer DEFAULT nextval('auto_increment_transaksi_spp'::regclass) NOT NULL,
    no_induk integer NOT NULL,
    tanggal date NOT NULL,
    dibayar numeric(12,0),
    bulan character varying(255),
    bangunan numeric(12,0),
    lain_lain numeric(12,0)
);


ALTER TABLE public.transaksi_spp OWNER TO imron;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: imron
--

COPY admin (username, password, nama, email, id) FROM stdin;
admin	21232f297a57a5a743894a0e4a801fc3	\N	\N	1
\.


--
-- Name: admin_sequence; Type: SEQUENCE SET; Schema: public; Owner: imron
--

SELECT pg_catalog.setval('admin_sequence', 1, false);


--
-- Name: auto_increment_admin; Type: SEQUENCE SET; Schema: public; Owner: imron
--

SELECT pg_catalog.setval('auto_increment_admin', 1, false);


--
-- Name: auto_increment_transaksi_spp; Type: SEQUENCE SET; Schema: public; Owner: imron
--

SELECT pg_catalog.setval('auto_increment_transaksi_spp', 58, true);


--
-- Data for Name: guru; Type: TABLE DATA; Schema: public; Owner: imron
--

COPY guru (no_induk_guru, nama, tlp, ttl, jk, ijazah, jabatan, mengajar, bidang_studi) FROM stdin;
\.


--
-- Name: sequence; Type: SEQUENCE SET; Schema: public; Owner: imron
--

SELECT pg_catalog.setval('sequence', 1, false);


--
-- Data for Name: siswa; Type: TABLE DATA; Schema: public; Owner: imron
--

COPY siswa (no_induk, nama, kelas, keterangan) FROM stdin;
2010140419	Imron Rosdiana	VII	\N
2010140423	Asih Restari	VIII	\N
\.


--
-- Data for Name: spp; Type: TABLE DATA; Schema: public; Owner: imron
--

COPY spp (kelas, spp, bangunan, lain_lain) FROM stdin;
Lulus	0	0	0
VII	140000	1000000	200000
VIII	150000	1000000	200000
IX	160000	1000000	200000
\.


--
-- Data for Name: transaksi_spp; Type: TABLE DATA; Schema: public; Owner: imron
--

COPY transaksi_spp (no_transaksi, no_induk, tanggal, dibayar, bulan, bangunan, lain_lain) FROM stdin;
45	2010140419	2013-10-16	0	\N	0	0
46	2010140423	2013-10-16	0	\N	0	0
53	2010140419	2013-10-22	\N	\N	100000	50000
55	2010140419	2013-10-22	\N	\N	100000	50000
56	2010140419	2013-10-22	\N	\N	800000	100000
58	2010140423	2013-10-27	\N	\N	100000	50000
\.


--
-- Name: pk_admin; Type: CONSTRAINT; Schema: public; Owner: imron; Tablespace: 
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT pk_admin PRIMARY KEY (id);


--
-- Name: pk_guru; Type: CONSTRAINT; Schema: public; Owner: imron; Tablespace: 
--

ALTER TABLE ONLY guru
    ADD CONSTRAINT pk_guru PRIMARY KEY (no_induk_guru);


--
-- Name: pk_siswa; Type: CONSTRAINT; Schema: public; Owner: imron; Tablespace: 
--

ALTER TABLE ONLY siswa
    ADD CONSTRAINT pk_siswa PRIMARY KEY (no_induk);


--
-- Name: pk_spp; Type: CONSTRAINT; Schema: public; Owner: imron; Tablespace: 
--

ALTER TABLE ONLY spp
    ADD CONSTRAINT pk_spp PRIMARY KEY (kelas);


--
-- Name: pk_transaksi_spp; Type: CONSTRAINT; Schema: public; Owner: imron; Tablespace: 
--

ALTER TABLE ONLY transaksi_spp
    ADD CONSTRAINT pk_transaksi_spp PRIMARY KEY (no_transaksi);


--
-- Name: _RETURN; Type: RULE; Schema: public; Owner: imron
--

CREATE RULE "_RETURN" AS ON SELECT TO transaksi DO INSTEAD SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.spp, sum(transaksi_spp.dibayar) AS dibayar, (spp.spp - sum(transaksi_spp.dibayar)) AS tunggakan, transaksi_spp.bulan FROM siswa, spp, transaksi_spp WHERE (((spp.kelas)::text = (siswa.kelas)::text) AND (siswa.no_induk = transaksi_spp.no_induk)) GROUP BY siswa.no_induk, spp.spp, transaksi_spp.bulan ORDER BY siswa.no_induk, siswa.kelas;


--
-- Name: fk_siswa; Type: FK CONSTRAINT; Schema: public; Owner: imron
--

ALTER TABLE ONLY siswa
    ADD CONSTRAINT fk_siswa FOREIGN KEY (kelas) REFERENCES spp(kelas);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

