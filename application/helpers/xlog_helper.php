<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('xlog')) {

    function xlog($data, $to_screen = true) {
        if ($to_screen) {
            echo '<pre class="-debug prettyprint">';
            print_r($data);
            echo '</pre>' . "\n";
        }

        log_message('info', print_r($data, true));
    }

}

/* End of file MY_xlog_helper.php */
/* Location: ./helpers/MY_xlog_helper.php */