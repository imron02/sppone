<?php doctype('html5'); ?>
<html lang="en">
<head>
	<title></title>
	<style type="text/css">
	body {
		margin: 0 auto;
		width: 700px;
	}

	table { 
	  width: 100%; 
	  border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
	  background: #eee; 
	}
	th { 
	  background: #333; 
	  color: white; 
	  font-weight: bold; 
	}
	td, th { 
	  padding: 6px; 
	  border: 1px solid #ccc; 
	  text-align: left; 
	}

	@page { 
		margin: 90px 30px; 
	}
    #footer-left { 
    	position: fixed; 
    	left: 0px; 
    	bottom: -180px; 
    	right: 0px; 
    	height: 140px; 
    }

    #footer-right { 
    	position: fixed; 
    	left: 0px; 
    	bottom: -180px; 
    	right: 0px; 
    	height: 140px; 
    	text-align: right;
    }

	#footer-left .page:after { 
	 	content: counter(page, upper-roman); 
	}

	</style>
</head>
<body>
	<h2><center>Laporan Transaksi<br />SMP Plus Darul Hikmah</center></h2>
	<table width="100%" border="1px" cellspacing="0">
		<thead>
	        <tr class="grid-head-row btn-inverse">
	            <th class="auto">No Induk</th>
	            <th class="auto">Nama</th>
	            <th class="auto">Kelas</th>
	            <th class="auto">Spp</th>
	            <th class="auto">Dibayar</th>
	            <th class="auto">Tunggakan</th>
	        </tr>
		</thead>
		<tbody>
	        <?php foreach ($hslquery as $row): ?>    
	        <tr>
	            <td><?php echo $row['no_induk']; ?></td>
	            <td><?php echo $row['nama']; ?></td>
	            <td><?php echo $row['kelas']; ?></td>
	            <td><?php echo $row['spp']; ?></td>
                <td><?php echo $row['dibayar']; ?></td>
                <td><?php echo $row['tunggakan']; ?></td>
	        </tr>
	        <?php endforeach; ?>
		</tbody>
    </table>

    <div id="footer-left">
     <p class="page">Page <?php $PAGE_NUM; ?></p>
   	</div>
   	<div id="footer-right">
   		<p>&copy; Imron Rosdiana 2013</p>
   	</div>
</body>
</html>