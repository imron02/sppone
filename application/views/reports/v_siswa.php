<style type="text/css">
    .kelas li:hover {
        color: #ffffff !important;
        text-decoration: none;
        background-color: #5DB9CC;
        background-image: -moz-linear-gradient(top, ##5DB9CC, #0077b3);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(##5DB9CC), to(#0077b3));
        background-image: -webkit-linear-gradient(top, ##5DB9CC, #0077b3);
        background-image: -o-linear-gradient(top, ##5DB9CC, #0077b3);
        background-image: linear-gradient(to bottom, ##5DB9CC, #0077b3);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5DB9CC', endColorstr='#ff0077b3', GradientType=0);
        display: block;
        padding: 3px 0px;
        clear: both;
        font-weight: normal;
        line-height: 20px;
        color: #333333;
        margin-left: -9%;
        margin-right: -10%;
        padding-left: 20px;
    }

    .kelas li {
        display: block;
        padding: 3px 0px 3px 5px;
        line-height: 20px;
    }

    
</style>
<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_reports/siswa'); ?>">Laporan Siswa</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_reports/siswa') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_reports/siswa_print_all') ?>" class="btn">Print PDF</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-left btn-group">
                    <!-- <button name="trash" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</button> -->
                    <!-- <a href="#" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</a>      -->       
                </div>
                <div class="pull-right">
                    <div class="filter-container">
                        <div class="input-prepend">
                          <div class="btn-group">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">Cetak
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu kelas">
                              <li>VII</li>
                              <li>VIII</li>
                              <li>IX</li>
                            </ul>
                          </div>
                          <input class="span2" type="text" name="nim" placeholder="No Induk">
                        </div>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="auto">
                                <a href="">No Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class='row-fluid grid-bottom'>
                <div class="span6 left">
                    <div class="page-changer pagination">
                        <div class="pull-left">
                            <span class="pull-left" style="padding-right: 5px">
                                Page
                            </span>
                            <?php echo $paginator; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
    $('.dropdown-menu li').click(function() {
        var kelas = $(this).text();
        var nim = $('input[name=nim]').val();
        var regex = new RegExp (/^\d{4,4}/);

        if(regex.test(nim) || nim == '') {
            window.location.replace("<?php echo site_url('c_reports/siswa_kelas'); ?>?kelas=" + kelas + '&nim=' + nim);
            return false;
        } else {
            alert("Silahkan input hanya angka dan minimal 4 angka..");
            return false;
        }
    });
})
</script>