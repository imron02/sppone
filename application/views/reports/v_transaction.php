<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_reports/transaction'); ?>">Laporan Transaksi</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_reports/transaction') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_reports/transaction_print') ?>" class="btn">Print PDF</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="auto">
                                <a href="">No.Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Spp</a>
                            </th>
                            <th class="auto">
                                <a href="">Dibayar</a>
                            </th>
                            <th class="auto">
                                <a href="">Tunggakan</a>
                            </th>
                            </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['spp']; ?></td>
                            <td><?php echo $row['dibayar']; ?></td>
                            <td><?php echo $row['tunggakan']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class='row-fluid grid-bottom'>
                <div class="span6 left">
                    <div class="page-changer pagination">
                        <div class="pull-left">
                            <span class="pull-left" style="padding-right: 5px">
                                Page
                            </span>
                            <?php echo $paginator; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>