<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_reports/transaction'); ?>">Pembayaran Spp</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_reports/transaction') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_reports/transaction_print_all') ?>" class="btn">Print PDF</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-right">
                    <div class="filter-container">
                        <?php echo form_open('c_spp/payment_search'); ?>
                            <div class="search input-append">
                                <input type="text" name="search" />
                                <input type="submit" class="btn" id="cari" value="Cari" />
                            </div>
                        <?php echo form_close(); ?>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="auto">
                                <a href="">No.Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Spp</a>
                            </th>
                            <th class="auto">
                                <a href="">Dibayar</a>
                            </th>
                            <th class="auto">
                                <a href="">Tunggakan</a>
                            </th>
                            <th class="grid-action-cell" style="width: 20px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['spp']; ?></td>
                            <td><?php echo $row['dibayar']; ?></td>
                            <td><?php echo $row['tunggakan']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit">
                                        <a href="<?php echo site_url('c_spp/payment_show_edit?edit='.$row['no_induk']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>