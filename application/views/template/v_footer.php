<style type="text/css">
.credit {
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 60px;
    text-align: center;
}
</style>
<div id="footer1">
        <p class="muted credit">
            <span class="big">
                Copyright © 2011 <a href="http://xinix.co.id" target="blank">Xinix Technology</a>. All rights reserved.
            </span>
            <span class="tiny">
                <a href="http://xinix.co.id" target="blank">Xinix Technology</a>
            </span>
        </p>
    </div>
    
  <script type="text/javascript" src="<?=base_url("assets/js/bootstrap.min.js");?>"></script>
  <script type="text/javascript" src="<?=base_url("assets/js/xn.js");?>"></script>
 </body>
</html>
