<?php doctype('html5'); ?>
<html lang="en">
 <head>
     <?php 
     echo meta($meta); 
     echo "<title>".$judul_lengkap.' - '.$instansi."</title>";
     echo link_tag('assets/css/bootstrap.min.css');
     echo link_tag('assets/css/custom.css');
     echo link_tag('assets/css/bootstrap-responsive.css');
     ?>
     <script type="text/javascript" src="<?=base_url("assets/js/jquery-1.8.2.min.js");?>"></script>
 </head>
<body>
    <!-- =================================== Navbar -->
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <i class="icon-th-list"></i>
                </a>
                <a href="<?php echo site_url('c_home'); ?>" class="brand">Aplikasi SPP</a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li><a href="<?php echo site_url("c_home"); ?>"><i class="icon-home">
                            </i></a>
                        </li>
                    </li>
                    </ul>
                    <ul class="nav pull-right">
                        <li>
                            <a href="#">
                            <span class="system-time">
                                <span class="xinix-date">15/07/2013</span> 
                                • 
                                <span class="xinix-time">11:20:02
                                </span>
                            </span>
                            </a>
                        </li>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Admin User <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('c_home/password') ?>">Change Password</a></li>
                            <li><a href="<?php echo site_url('c_home/logout?continue=').current_url(); ?>">Logout</a></li>
                        </ul>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>