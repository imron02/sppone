<?php doctype('html5'); ?>
<html lang="en">
 <head>
     <?php 
     echo meta($meta); 
     echo "<title>".$judul_lengkap.' - '.$instansi."</title>";
     echo link_tag('assets/css/bootstrap.min.css');
     echo link_tag('assets/css/login.css');
     echo link_tag('assets/css/bootstrap-responsive.css');
     ?>
     <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .big {
        text-shadow: 0 0 10px #000;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 10% auto 20px;
        background: rgba(149, 188, 228, 0.5);
        border: 1px solid #95BCE4;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }

      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }

      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

      img {
        display: block;
        margin-left: auto;
        margin-right: auto;
      }

      .alert-block {
        background: rgba(149, 188, 228, 0.5);
        color: blue;
        border: none;
      }
    </style>
 </head>
<body class="login">

    <div class="container">
        <?php if(validation_errors()) { ?>
            <div class="alert alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <h4>Terjadi Kesalahan!</h4>
                <?php echo validation_errors(); ?>
            </div>
        <?php } ?>

        
        <form action="<?php echo site_url('c_login/login'); ?>" method="post" accept-charset="utf-8" class="form-signin">
            <h2 class="form-signin-heading">Please sign in</h2>
            <input type="text" class="input-block-level" placeholder="Username" name="username">
            <input type="password" name="password" class="input-block-level" placeholder="Password">
            <input type="hidden" name="continue" value="<?php echo $this->input->get('continue'); ?>">
            <button class="btn btn-large btn-primary" type="submit">Sign in</button>
        </form>

        <div id="footer">
            <p class="muted credit">
                <span class="big">
                Copyright © 2013 <a href="http://imron02.blogspot.com" target="blank">Imron02 Technology</a>. All rights reserved.
                </span>
                <span class="tiny">
                    <a href="http://xinix.co.id" target="blank">Imron02 Technology</a>
                </span>
            </p>
        </div>
    </div> <!-- /container -->

    <script type="text/javascript" src="<?=base_url("assets/js/jquery.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/bootstrap.min.js");?>"></script>
</body>
</html>
