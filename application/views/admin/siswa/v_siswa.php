<script type="text/javascript">
$(function(){
    //check uncheck
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) 
    $('input:checkbox').prop('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });

    $("button[name=trash]").click(function() {

        var allcheck = [];
        $("input[name=rowdel]:checked").each(function() {
            allcheck.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_siswa/trash'); ?>",
            data: "data=" + allcheck
        })
        .done(function() {
            alert('Success Delete');
            $('body').load("<?php echo site_url('c_siswa'); ?>");
        })
        .fail(function() {alert('Error Delete'); });
    });

    $("button[name=lulus]").click(function() {
        var allcheck = [];
        $("input[name=rowdel]:checked").each(function() {
            allcheck.push($(this).val());
        });
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_siswa/lulus'); ?>",
            data: "data=" + allcheck
        })
        .done(function() {
            alert('Success Edit');
            $('body').load("<?php echo site_url('c_siswa'); ?>");
        })
        .fail(function() {alert('Error Delete'); });
    });

});
</script>
<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_siswa'); ?>">Siswa</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_siswa') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_siswa/siswa') ?>" class="btn">Tambah</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-left btn-group">
                    <button name="trash" class="btn btn-danger mass-action" id="btn-danger">Hapus</button>
                    <button class="btn btn-primary mass-action" name="lulus" id="btn-primary">Lulus</button>       
                </div>
                <div class="pull-right">
                    <div class="filter-container">
                        <?php echo form_open('c_siswa/search'); ?>
                            <div class="search input-append">
                                <input type="text" name="search" />
                                <input type="submit" class="btn" id="cari" value="Cari" />
                            </div>
                        <?php echo form_close(); ?>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="xx-short" style="width: 1px"><input type="checkbox" name="all" id="checkall" /></th>
                            <th class="auto">
                                <a href="">No Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Keterangan</a>
                            </th>
                            <th class="grid-action-cell" style="width: 38px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><input type="checkbox" class="grid_body" name="rowdel" value="<?php echo $row['no_induk'];?>" /></td>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['keterangan']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit">
                                        <a href="<?php echo site_url('c_siswa/show_edit?edit='.$row['no_induk']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                    <span class="remove">
                                        <a href="<?php echo site_url('c_siswa/delete?delete='.$row['no_induk']); ?>" class="grid-action  icon-remove"></a>
                                    </span>                                                                        
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class='row-fluid grid-bottom'>
                <div class="span6 left">
                    <div class="page-changer pagination">
                        <div class="pull-left">
                            <span class="pull-left" style="padding-right: 5px">
                                Page
                            </span>
                            <?php echo $paginator; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>