<style type="text/css">
legend {
	text-align: left;
}

input[type="text"] {
	padding: 14px;
}
</style>
<div class="container-fluid">
	<?php if(validation_errors()) { ?>
    <div class="alert alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <h4>Terjadi Kesalahan!</h4>
        <?php echo validation_errors(); ?>
        
    </div>
    <?php } ?>

    <div class="wrap">
		<div class="container-fluid" id="container">
			<div class="header">
				<div class="pull-left">
					<ul class="breadcrumb">
	                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
	                    </li>
	                    <li><a href="<?php echo site_url('c_siswa'); ?>">Siswa</a>
	                    </li>
	                    <li class="active"><a href="">Edit</a></li>
	                </ul>
				</div>
			</div>

			<div class="clearfix"></div>
			<?php echo form_open('c_siswa/edit'); ?>
			<fieldset>
				<legend>Edit Siswa</legend>
				<div id="field">
					<table class="grid table table-hover table-striped table-condesed" id="field_grid">
						<tr>
							<th>No. Induk</th>
							<th>Nama</th>
							<th>Kelas</th>
						</tr>
						<tr>
							<td>
                        		<input type="text" value="<?php echo $hslquery['no_induk']; ?>" class="field_field" name="noInduk" disabled />
                        		<input type="hidden" value="<?php echo $hslquery['no_induk']; ?>" class="field_field" name="noInduk" />
                    		</td>
                    		<td>
                    			<input type="text" value="<?php echo $hslquery['nama']; ?>" class="field_field"  name="nama" required pattern="[a-zA-Z][a-zA-Z ]+" />
                    		</td>
                    		<td>
                    			<?php
						            $options = array('VII' => 'VII', 'VIII' => 'VIII', 'IX' => 'IX', 'Lulus' => 'Lulus');
						            echo form_dropdown('kelas', $options, $hslquery['kelas']);
					            ?>
                    		</td>
						</tr>
					</table>
				</div>
				<div class="action-buttons btn-group">
					<input type="submit" class="btn btn-primary" />
					<input type="reset" class="btn" id="add_field" value="Reset" />
				</div>
			</fieldset>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>