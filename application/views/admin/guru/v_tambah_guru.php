<style type="text/css">
legend {
    text-align: left;
}

input[type="text"] {
    padding: 14px;
}

i {
    font-style: normal !important; 
}
</style>

<script type="text/javascript">

function addField() {
        var $row = $($('#template').html());
        $('#field_grid tbody').append($row);

        $('#field_grid').find("tr:nth-child(odd)").removeClass('even').removeClass('odd').addClass("odd");
        $('#field_grid').find("tr:nth-child(even)").removeClass('even').removeClass('odd').addClass("even");
    }

function removeField($o) {
    if ($o.parents('tbody').find('.removetr').length <= 2) {
        addField();
    }
    $('.removetr').remove();
}

$('#add_field').live('click', function(evt) {
    addField();
    return evt.preventDefault(); 
});

$('.btn-remove').live('click', function(evt) {
    removeField($(this));
    return evt.preventDefault();
});

$('.btn-up').live('click', function(evt) {
    var current = $(this).parents('tr');
    current.prev().before(current);
    return evt.preventDefault();
});

$('.btn-down').live('click', function(evt) {
    var current = $(this).parents('tr');
    current.next().after(current);
    return evt.preventDefault();
});
</script>
<div class="container-fluid">
    <div class="wrap">
        <div class="container-fluid" id="container">
            <div class="header">
                <div class="pull-left">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                        </li>
                        <li><a href="<?php echo site_url('c_guru'); ?>">Guru</a>
                        </li>
                        <li class="active"><a href="<?php echo site_url('c_guru/guru') ?>">Add</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php echo form_open('c_guru/add_guru'); ?>
    <div class="grid-container table-bordered">
    <fieldset>
        <legend>Tambah Guru</legend>
        <div id="field">
            <table class="grid table table-hover table-striped table-condesed" id="field_grid">
                <tr class="odd">
                    <th>No. Induk</th>
                     <td>
                        <input type="text" id="noInduk" name="noInduk[]" required pattern="[0-9]+" placeholder="No Induk" />
                    </td>
                    <th>Nama</th>
                    <td>
                        <input type="text"  name="nama[]" required placeholder="Nama" />
                    </td>
                </tr>
                <tr class="odd">
                    <th>Tempat, Tanggal Lahir</th>
                    <td>
                        <input type="text"  name="ttl[]" placeholder="Tempat, Tanggal Lahir" />
                    </td>
                    <th>Jenis Kelamin</th>
                    <td>
                        <select name="jk[]" required>
                            <option value=""></option>
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </td>
                </tr>
                <tr class="odd">
                    <th>Ijazah Terakhir</th>
                    <td>
                        <input type="text"  name="ijazah[]"  placeholder="Ijazah Terakhir" />
                    </td>
                    <th>Jabatan</th>
                    <td>
                        <input type="text"  name="jabatan[]"  placeholder="Jabatan" />
                    </td>
                </tr>
                <tr class="odd">
                    <th>Mulai Mengajar</th>
                    <td>
                        <input type="text"  name="mengajar[]"  pattern="[0-9]+" placeholder="Mulai Mengajar" />
                    </td>
                    <th>Mengajar Bidang Studi</th>
                    <td>
                        <input type="text"  name="bidang_studi[]"  placeholder="Bidang Studi" />
                    </td>
                </tr>
                <tr class="odd">
                    <th>Telepon</th>
                    <td>
                        <input type="text"  name="tlp[]" placeholder="Telepon" />
                    </td>
                    <td colspan="2"></td>
                </tr>
            </table>
    </div><!-- ./grid-container -->
        </div>
        <div class="action-buttons btn-group">
                <input type="submit" class="btn btn-primary" />
                <a href="#" class="btn" id="add_field">Add Field</a>
            </div>
    </fieldset>
    <?php echo form_close(); ?>
    <script type="text/template" id="template">
    <tr class="odd removetr">
        <td colspan="4" style="height: 30px;"></td>
    </tr>
    <tr class="odd removetr">
        <th>No. Induk</th>
         <td>
            <input type="text" id="noInduk" name="noInduk[]" required pattern="[0-9]+" placeholder="No Induk" />
        </td>
        <th>Nama</th>
        <td>
            <input type="text"  name="nama[]" required placeholder="Nama" />
        </td>
    </tr>
    <tr class="odd removetr">
        <th>Tempat, Tanggal Lahir</th>
        <td>
            <input type="text"  name="ttl[]" placeholder="Tempat, Tanggal Lahir" />
        </td>
        <th>Jenis Kelamin</th>
        <td>
            <select name="jk[]" required>
                <option value=""></option>
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
            </select>
        </td>
    </tr>
    <tr class="odd removetr">
        <th>Ijazah Terakhir</th>
        <td>
            <input type="text"  name="ijazah[]"  placeholder="Ijazah Terakhir" />
        </td>
        <th>Jabatan</th>
        <td>
            <input type="text"  name="jabatan[]"  placeholder="Jabatan" />
        </td>
    </tr>
    <tr class="odd removetr">
        <th>Mulai Mengajar</th>
        <td>
            <input type="text"  name="mengajar[]"  pattern="[0-9]+" placeholder="Mulai Mengajar" />
        </td>
        <th>Mengajar Bidang Studi</th>
        <td>
            <input type="text"  name="bidang_studi[]"  placeholder="Bidang Studi" />
        </td>
    </tr>
    <tr class="odd removetr">
        <th>Telepon</th>
        <td>
            <input type="text"  name="tlp[]" placeholder="Telepon" />
        </td>
        <th></th>
        <td>
            <a href="#" class="btn-remove">Delete</a>
        </td>
    </tr>
</script>
<script type="text/javascript">
$(function(){
    //function if submit is clicked
    $('input[type=submit]').click(function(){
        var noInduk = [];
        $('input[id=noInduk]').each(function() {
            noInduk.push($(this).val());
        });
        
    //get objeck for No Induk error
        var result = ''
        result = $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_guru/check_guru'); ?>",
            data: "data=" + noInduk,
            dataType: 'html',
            ontext: document.body,
            global: false,
            async:false,
            success:function (data) {
                return data;
            }
        }).responseText;

    //validation objek
        var obj = jQuery.parseJSON(result);
        if(obj.length > 0) {
            alert('No Induk ' + obj + ' telah ada');    
            return false;
        } else {
            return true;
        }
        
    });

});
</script>
</div>