<script type="text/javascript">
$(function(){
    //check uncheck
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) 
    $('input:checkbox').attr('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });

    $("button[name=trash]").click(function() {

        var allcheck = [];
        $("input[name=rowdel]:checked").each(function() {
            allcheck.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_guru/trash'); ?>",
            data: "data=" + allcheck
        })
        .done(function() {
            alert('Success Delete');
            $('body').load("<?php echo site_url('c_guru'); ?>");
        })
        .fail(function() {alert('Error Delete'); });
    });

});
</script>
<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_guru'); ?>">Guru</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_guru') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_guru/guru') ?>" class="btn">Tambah</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-left btn-group">
                    <button name="trash" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</button>
                    <!-- <a href="#" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</a>      -->       
                </div>
                <div class="pull-right">
                    <div class="filter-container">
                        <?php echo form_open('c_guru/search'); ?>
                            <div class="search input-append">
                                <input type="text" name="search" />
                                <input type="submit" class="btn" id="cari" value="Cari" />
                            </div>
                        <?php echo form_close(); ?>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="xx-short" style="width: 1px"><input type="checkbox" name="all" id="checkall" /></th>
                            <th class="auto">
                                <a href="">No Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Tempat, Tanggal Lahir</a>
                            </th>
                            <th class="auto">
                                <a href="">Jenis Kelamin</a>
                            </th>
                            <th class="auto">
                                <a href="">Ijazah Terakhir</a>
                            </th>
                            <th class="auto">
                                <a href="">Jabatan</a>
                            </th>
                            <th class="auto">
                                <a href="">Mulai Mengajar</a>
                            </th>
                            <th class="auto">
                                <a href="">Mengajar Bidang Studi</a>
                            </th>
                            <th class="auto">
                                <a href="">Telepon</a>
                            </th>
                            <th class="grid-action-cell" style="width: 38px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><input type="checkbox" class="grid_body" name="rowdel" value="<?php echo $row['no_induk_guru'];?>" /></td>
                            <td><?php echo $row['no_induk_guru']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['ttl']; ?></td>
                            <td><?php echo $row['jk']; ?></td>
                            <td><?php echo $row['ijazah']; ?></td>
                            <td><?php echo $row['jabatan']; ?></td>
                            <td><?php echo $row['mengajar']; ?></td>
                            <td><?php echo $row['bidang_studi']; ?></td>
                            <td><?php echo $row['tlp']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit">
                                        <a href="<?php echo site_url('c_guru/show_edit?edit='.$row['no_induk_guru']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                    <span class="remove">
                                        <a href="<?php echo site_url('c_guru/delete?delete='.$row['no_induk_guru']); ?>" class="grid-action  icon-remove"></a>
                                    </span>                                                                        
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class='row-fluid grid-bottom'>
                <div class="span6 left">
                    <div class="page-changer pagination">
                        <div class="pull-left">
                            <span class="pull-left" style="padding-right: 5px">
                                Page
                            </span>
                            <?php echo $paginator; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>