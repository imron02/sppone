<style type="text/css">
legend {
    text-align: left;
}

input[type="text"] {
    padding: 14px;
}

i {
    font-style: normal !important; 
}
</style>

<script type="text/javascript">

function addField() {
        var $row = $($('#template').html());
        $('#field_grid tbody').append($row);
        // xn.helper.stylize($row);
        // $('#field_grid tr:last').clone().appendTo('#field_grid tbody');

        $('#field_grid').find("tr:nth-child(odd)").removeClass('even').removeClass('odd').addClass("odd");
        $('#field_grid').find("tr:nth-child(even)").removeClass('even').removeClass('odd').addClass("even");
    }

function removeField($o) {
    if ($o.parents('tbody').find('tr').length <= 2) {
        addField();
    }
    $o.parents('tr').remove();
}

$('#add_field').live('click', function(evt) {
    addField();
    return evt.preventDefault(); 
});

$('.btn-remove').live('click', function(evt) {
    removeField($(this));
    return evt.preventDefault();
});

$('.btn-up').live('click', function(evt) {
    var current = $(this).parents('tr');
    current.prev().before(current);
    return evt.preventDefault();
});

$('.btn-down').live('click', function(evt) {
    var current = $(this).parents('tr');
    current.next().after(current);
    return evt.preventDefault();
});
</script>
<div class="container-fluid">
    <div class="wrap">
        <div class="container-fluid" id="container">
            <div class="header">
                <div class="pull-left">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                        </li>
                        <li><a href="<?php echo site_url('c_spp'); ?>">Spp</a>
                        </li>
                        <li class="active"><a href="<?php echo site_url('c_spp/spp') ?>">Add</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php echo form_open('c_spp/add_spp'); ?>
    <fieldset>
        <legend>Tambah Spp</legend>
        <div id="field">
            <table class="grid table table-hover table-striped table-condesed" id="field_grid">
                <tr class="odd">
                    <th>Kelas</th>
                    <th>Spp</th>
                    <th>Bangunan</th>
                    <th>Lain-lain</th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <select name="kelas[]">
                            <option value="VII">VII</option>
                            <option value="VIII">VIII</option>
                            <option value="IX">IX</option>
                        </select>
                    </td>
                    <td>
                        <input type="text"  name="spp[]" required pattern="[/\d/]+$" placeholder="Spp" />
                    </td>
                    <td>
                        <input type="text"  name="bangunan[]" required pattern="[/\d/]+$" placeholder="Bangunan" />
                    </td>
                    <td>
                        <input type="text"  name="lain-lain[]" required pattern="[/\d/]+$" placeholder="Lain-lain" />
                    </td>
                    <td>
                        <a href="#" class="btn-remove">Delete</a>
                        <a href="#" class="btn-up">Up</a>
                        <a href="#" class="btn-down">Down</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="action-buttons btn-group">
                <input type="submit" class="btn btn-primary" />
                <a href="#" class="btn" id="add_field">Add Field</a>
            </div>
    </fieldset>
    <?php echo form_close(); ?>
    <script type="text/template" id="template">
    <tr>
        <td>
            <select name="kelas[]">
                <option value="VII">VII</option>
                <option value="VIII">VII</option>
                <option value="IX">IX</option>
            </select>
        </td>
        <td>
            <input type="text"  name="spp[]" pattern="[/\d/]+$" placeholder="Spp" required />
        </td>
        <td>
            <input type="text"  name="bangunan[]" required pattern="[/\d/]+$" placeholder="Bangunan" />
        </td>
        <td>
            <input type="text"  name="lain-lain[]" required pattern="[/\d/]+$" placeholder="Lain-lain" />
        </td>
        <td>
            <a href="#" class="btn-remove">Delete</a>
            <a href="#" class="btn-up">Up</a>
            <a href="#" class="btn-down">Down</a>
        </td>
    </tr>
</script>
<script type="text/javascript">
$(function(){
    //function if submit is clicked
    $('input[type=submit]').click(function(){
        var kelas = [];
        $('input[id=kelas]').each(function() {
            kelas.push($(this).val());
        });
        
    //get objeck for No Induk error
        var result = ''
        result = $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_spp/check_spp'); ?>",
            data: "data=" + kelas,
            dataType: 'html',
            ontext: document.body,
            global: false,
            async:false,
            success:function (data) {
                return data;
            }
        }).responseText;
        
    //validation objek
        var obj = jQuery.parseJSON(result);
        if(obj.length > 0) {
            alert('Kelas ' + obj + ' telah ada');    
            return false;
        } else {
            return true;
        }
        
    });

});
</script>
</div>