<style type="text/css">
legend {
    text-align: left;
}

input[type="text"] {
    padding: 14px;
}

i {
    font-style: normal !important; 
}
</style>

<div class="container-fluid">
    <div class="wrap">
        <div class="container-fluid" id="container">
            <div class="header">
                <div class="pull-left">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                        </li>
                        <li><a href="<?php echo site_url('c_spp/building'); ?>">Pembayaran Bangunan</a>
                        </li>
                        <li class="active"><a href="<?php echo site_url('c_spp/building_payment?data=' . $no_induk); ?>">Bayar</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php echo form_open('c_spp/building_payment'); ?>
        <fieldset>
            <legend>Bayar Bangunan &amp; Lain-lain</legend>
            <div id="field">
                <table class="grid table table-hover table-striped table-condesed" id="field_grid">
                    <tr class="odd">
                        <th>Bangunan</th>
                        <td>
                            <input type="text"  name="bangunan" class="field_field" required pattern="[/\d/]+$" placeholder="Bangunan" />
                        </td>
                    </tr>
                    <tr>
                        <th>Lain-lain</th>
                        <td>
                            <input type="text"  name="lain_lain" class="field_field" required pattern="[/\d/]+$" placeholder="Lain-lain" />
                            <input type="hidden" name="no_induk" value="<?php echo $no_induk ?>" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="action-buttons btn-group">
                <input type="submit" class="btn btn-primary" />
                <input type="Reset" class="btn" id="add_field" value="Reset" />
            </div>
        </fieldset>
    <?php echo form_close(); ?>
</div>