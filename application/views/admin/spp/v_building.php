<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_spp/building'); ?>">Pembayaran Bangunan</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_spp/building') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-right">
                    <div class="filter-container">
                        <?php echo form_open('c_spp/building_search'); ?>
                            <div class="search input-append">
                                <input type="text" name="search" />
                                <input type="submit" class="btn" id="cari" value="Cari" />
                            </div>
                        <?php echo form_close(); ?>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="auto">
                                <a href="">No.Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Bangunan</a>
                            </th>
                            <th class="auto">
                                <a href="">Bayar Bangunan</a>
                            </th>
                            <th class="auto">
                                <a href="">Tunggakan Bangunan</a>
                            </th>
                            <th class="auto">
                                <a href="">Lain-lain</a>
                            </th>
                            <th class="auto">
                                <a href="">Bayar Lain</a>
                            </th>
                            <th class="auto">
                                <a href="">Tunggakan Lain</a>
                            </th>
                            <th class="grid-action-cell" style="width: 20px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['bangunan']; ?></td>
                            <td><?php echo $row['bayar_bangunan']; ?></td>
                            <td><?php echo $row['tunggakan_bangunan']; ?></td>
                            <td><?php echo $row['lain_lain']; ?></td>
                            <td><?php echo $row['bayar_lain']; ?></td>
                            <td><?php echo $row['tunggakan_lain']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit">
                                        <a href="<?php echo site_url('c_spp/building_show_edit?edit='.$row['no_induk']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class='row-fluid grid-bottom'>
                <div class="span6 left">
                    <div class="page-changer pagination">
                        <div class="pull-left">
                            <span class="pull-left" style="padding-right: 5px">
                                Page
                            </span>
                            <?php echo $paginator; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>