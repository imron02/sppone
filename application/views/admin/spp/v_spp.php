<script type="text/javascript">
$(function(){
    //check uncheck
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) 
    $('input:checkbox').attr('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });

    $("button[name=trash]").click(function() {

        var allcheck = [];
        $("input[name=rowdel]:checked").each(function() {
            allcheck.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_spp/trash'); ?>",
            data: "data=" + allcheck
        })
        .done(function() {
            alert('Success Delete');
            $('body').load("<?php echo site_url('c_spp'); ?>");
        })
        .fail(function() {alert('Error Delete'); });
    });

});
</script>
<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_spp'); ?>">Spp</a>
                    </li>
                    <li class="active"><a href="<?php echo site_url('c_spp') ?>">Listing</a></li>
                </ul>
            </div>
            <div class="pull-right btn-group">
                <a href="<?php echo site_url('c_spp/spp') ?>" class="btn">Tambah</a>    
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-left btn-group">
                    <button name="trash" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</button>
                    <!-- <a href="#" class="btn btn-danger mass-action" name="trash" id="btn-danger">Hapus</a>      -->       
                </div>
                <div class="pull-right">
                    <div class="filter-container">
                        <?php echo form_open('c_spp/search'); ?>
                            <div class="search input-append">
                                <input type="text" name="search" />
                                <input type="submit" class="btn" id="cari" value="Cari" />
                            </div>
                        <?php echo form_close(); ?>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="xx-short" style="width: 1px"><input type="checkbox" name="all" id="checkall" /></th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Total Pembayaran</a>
                            </th>
                            <th class="auto">
                                <a href="">Biaya Perbulan</a>
                            </th>
                            <th class="auto">
                                <a href="">Biaya Bangunan</a>
                            </th>
                            <th class="auto">
                                <a href="">Biaya Lain-lain</a>
                            </th>
                            <th class="grid-action-cell" style="width: 38px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>    
                        <tr>
                            <td><input type="checkbox" class="grid_body" name="rowdel" value="<?php echo $row['kelas'];?>" /></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['spp']; ?></td>
                            <td><?php echo $row['perbulan']; ?></td>
                            <td><?php echo $row['bangunan']; ?></td>
                            <td><?php echo $row['lain_lain']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit">
                                        <a href="<?php echo site_url('c_spp/show_edit?edit='.$row['kelas']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                    <span class="remove">
                                        <a href="<?php echo site_url('c_spp/delete?delete='.$row['kelas']); ?>" class="grid-action  icon-remove"></a>
                                    </span>                                                                        
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>