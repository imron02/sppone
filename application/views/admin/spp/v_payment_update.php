<style type="text/css">
legend {
    text-align: left;
}

input[type="text"] {
    padding: 14px;
}

i {
    font-style: normal !important; 
}

select[name="month"] {
    width: 100%;
}
</style>
<div class="container-fluid">
	<div class="wrap">
		<div class="container-fluid" id="container">
			<div class="header">
				<div class="pull-left">
					<ul class="breadcrumb">
	                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
	                    </li>
	                    <li><a href="<?php echo site_url('c_spp/payment_show_edit?edit=').$noInduk ?>">Transaksi Spp</a>
	                    </li>
	                    <li class="active"><a href="">Update Spp</a></li>
	                </ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
    <fieldset>
        <legend>Pembayaran</legend>
        <?php echo form_open('c_spp/payment_update'); ?>
        <div id="field">
            <table class="grid table table-hover table-striped table-condesed" id="field_grid" width="100%">
                <thead>
                    <tr>
                        <th width="10%">No. Induk</th>
                        <td width="4%">:</td>
                        <th width="86%"><?php echo $noInduk; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Bulan</th>
                        <td>:</td>
                        <td><?php echo form_dropdown('month', $options,'',"class='month'"); ?>
                            <input type='hidden' name="update" value="<?php echo $update; ?>" />
                            <input type='hidden' name="noInduk" value="<?php echo $noInduk; ?>" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="action-buttons btn-group">
                <input type="submit" class="btn btn-primary" />
        </div>
        <?php echo form_close(); ?>
    </fieldset>
</div>
<script type="text/javascript">
    $(function(){

        $('input[type=submit').click(function() {
            var success = true;
            $('.month option:selected').each(function() {
                if($(this).val() == "0") {
                    alert("Maaf bulan tidak boleh ada yg kosong");
                    success = false;
                    return false;
                }
            });

            if(success) {
                return true;
            }
            return false;
        });
    });
</script>