<style type="text/css">
legend {
	text-align: left;
}

input[type="text"] {
	padding: 14px;
}
</style>
<div class="container-fluid">
	<?php if(validation_errors()) { ?>
    <div class="alert alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <h4>Terjadi Kesalahan!</h4>
        <?php echo validation_errors(); ?>
        
    </div>
    <?php } ?>

    <div class="wrap">
		<div class="container-fluid" id="container">
			<div class="header">
				<div class="pull-left">
					<ul class="breadcrumb">
	                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
	                    </li>
	                    <li><a href="<?php echo site_url('c_spp'); ?>">Spp</a>
	                    </li>
	                    <li class="active"><a href="">Edit</a></li>
	                </ul>
				</div>
			</div>

			<div class="clearfix"></div>
			<?php echo form_open('c_spp/show_edit'); ?>
			<fieldset>
				<legend>Edit Spp</legend>
				<div id="field">
					<table class="grid table table-hover table-striped table-condesed" id="field_grid">
						<tr>
							<th>Kelas</th>
							<th>Spp</th>
							<th>Bangunan</th>
							<th>Lain-lain</th>
						</tr>
						<tr>
							<td>
                        		<input type="text" value="<?php echo $hslquery['kelas']; ?>" name="noInduk" disabled />
                        		<input type="hidden" value="<?php echo $hslquery['kelas']; ?>" name="kelas" />
                    		</td>
                    		<td>
                    			<input type="text" value="<?php echo $hslquery['spp']; ?>"  name="spp" required pattern="[/\d/]+$" />
                    		</td>
                    		<td>
                    			<input type="text" value="<?php echo $hslquery['bangunan']; ?>"  name="bangunan" required pattern="[/\d/]+$" />
                    		</td>
                    		<td>
                    			<input type="text" value="<?php echo $hslquery['lain_lain']; ?>"  name="lain_lain" required pattern="[/\d/]+$" />
                    		</td>
						</tr>
					</table>
				</div>
				<div class="action-buttons btn-group">
					<input type="submit" class="btn btn-primary" />
					<input type="reset" class="btn" id="add_field" value="Reset" />
				</div>
			</fieldset>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>