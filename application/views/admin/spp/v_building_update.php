<style type="text/css">
    legend {
        text-align: left;
    }

    input[type="text"] {
        padding: 14px;
    }

    i {
        font-style: normal !important; 
    }

    select[name="month"] {
        width: 100%;
    }

    input[type="submit"] {
        top: 10px;
    }

</style>
<div class="container-fluid">
	<div class="wrap">
		<div class="container-fluid" id="container">
			<div class="header">
				<div class="pull-left">
					<ul class="breadcrumb">
	                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
	                    </li>
	                    <li><a href="<?php echo site_url('c_spp/building_show_edit?edit=').$noInduk ?>">Transaksi Bangunan</a>
	                    </li>
	                    <li class="active"><a href="">Update Bangunan</a></li>
	                </ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
    <fieldset>
        <legend>Edit Pembayaran</legend>
        <?php echo form_open('c_spp/building_update'); ?>
        <div id="field">
            <table class="grid table table-hover table-striped table-condesed" id="field_grid" width="100%">
                <thead>
                    <tr>
                        <th width="10%">No. Induk</th>
                        <td width="4%">:</td>
                        <th width="86%"><?php echo $noInduk; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Bangunan</th>
                        <td>:</td>
                        <td><input type="text"  name="bangunan" class="field_field" required pattern="[/\d/]+$" value="<?php echo $hslquery['bangunan'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Lain-lain</th>
                        <td>:</td>
                        <td><input type="text"  name="lain_lain" class="field_field" required pattern="[/\d/]+$" value="<?php echo $hslquery['lain_lain'] ?>" />
                            <input type="hidden" name="noTransaksi" value="<?php echo $hslquery['no_transaksi']; ?>" />
                            <input type="hidden" name="noInduk" value="<?php echo $noInduk; ?>" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="action-buttons btn-group">
                <input type="submit" class="btn btn-primary" />
        </div>
        <?php echo form_close(); ?>
    </fieldset>
</div>