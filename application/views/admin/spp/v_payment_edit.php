<script type="text/javascript">
$(function(){
    $('.alert').hide();
    //check uncheck
    $('#checkall:checkbox').change(function () {
        if($(this).attr("checked")) 
    $('input:checkbox').attr('checked','checked');
        else $('input:checkbox').removeAttr('checked');
    });

    $("button[name=trash]").click(function(e) {
        var noInduk = "<?php echo $hslquery[0]['no_induk']; ?>";
        var allcheck = [];
        $("input[name=rowdel]:checked").each(function() {
            allcheck.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('c_spp/payment_trash'); ?>",
            data: "data=" + allcheck
        })
        .done(function() {
            alert('Success Delete');
            $('body').load("<?php echo site_url('c_spp/payment_show_edit?edit='); ?>"+ noInduk);
        })
        .fail(function() {alert('Error Delete'); });
    });

    $(".bayar").click(function() {
        var bulan = prompt("Mau bayar berapa bulan?");
        var regex = new RegExp (/^\d+$/);
        if(regex.test(bulan)) {
            window.location.replace("<?php echo site_url('c_spp/payment_show_add?bayar='.$hslquery[0]['no_induk']); ?>&bulan=" 
                + bulan + "&perbulan=<?php echo $hslquery1['perbulan']; ?>");
            return false;
        } else {
            alert("Silahkan input hanya angka..");
            return false;
        }
    });

    $('.update:first').click(function(e) {
        $('#alert').append('Error! Silahkan edit data yang lain.');
        $('.alert').show();
        e.preventDefault();
    });

    $('.remove:first').click(function(e) {
        $('#alert').append('Error! Silahkan hapus data yang lain.');
        $('.alert').show();
        e.preventDefault();
    });

});
</script>
<div class="wrap">
    <div class="container-fluid" id="container">
        <div class="alert alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Terjadi Kesalahan!</h4>
            <p id='alert'></p>
        </div>
        <div class="header">
            <div class="pull-left">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_spp/payment'); ?>">Pembayaran Spp</a>
                    </li>
                    <li class="active"><a href="">Listing</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div class="grid-top">
                <div class="pull-left btn-group">
                    <button name="trash" class="btn btn-danger mass-action" id="btn-danger">Hapus</button>
                </div>
                <div class="pull-right">
                    <div class="filter-container">
                         <a href="" class="btn bayar">Bayar</a>
                    </div>            
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row-fluid">
                <div class="span12"><b style="text-transform: uppercase; color: color: #9CCBC8;">Tunggakan: <?php echo $hslquery1['tunggakan'] ?>, 
                    Biaya perbulan: <?php echo $hslquery1['perbulan']; ?></b></div>
            </div>

            <div class="grid-container table-bordered">
                <table class="grid table table-hover table-striped table-condensed" id="example">
                    <thead>
                        <tr class="grid-head-row btn-inverse">
                            <th class="xx-short" style="width: 1px"><input type="checkbox" name="all" id="checkall" /></th>
                            <th class="auto">
                                <a href="">No Induk</a>
                            </th>
                            <th class="auto">
                                <a href="">Nama</a>
                            </th>
                            <th class="auto">
                                <a href="">Kelas</a>
                            </th>
                            <th class="auto">
                                <a href="">Tanggal</a>
                            </th>
                            <th class="auto">
                                <a href="">Dibayar</a>
                            </th>
                            <th class="auto">
                                <a href="">Bulan</a>
                            </th>
                            <th class="grid-action-cell" style="width: 38px">
                                <span>&nbsp;</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($hslquery as $row): ?>
                        <tr>
                            <td><input type="checkbox" class="grid_body" name="rowdel" value="<?php echo $row['no_transaksi'];?>" /></td>
                            <td><?php echo $row['no_induk']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['kelas']; ?></td>
                            <td><?php echo $row['tanggal']; ?></td>
                            <td><?php echo $row['dibayar']; ?></td>
                            <td><?php echo $row['bulan']; ?></td>
                            <td class="submenu">
                                <div class="submenu-container">
                                    <span class="edit update">
                                        <a href="<?php echo site_url('c_spp/payment_show_update?update='.$row['no_transaksi'].'&noInduk='.$row['no_induk']); ?>" class="grid-action icon-edit"></a>
                                    </span>
                                    <span class="remove">
                                        <a href="<?php echo site_url('c_spp/payment_delete?id='.$row['no_transaksi']).'&edit='.$row['no_induk']; ?>" class="grid-action icon-remove delete"></a>
                                    </span>                                                                        
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>