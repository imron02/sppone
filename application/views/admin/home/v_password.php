<style type="text/css">
	.bottom-1 {
		padding-bottom: 10px;
	}

	div .error {
		border-color: #e9322d !important;
		box-shadow: 0 0 6px #f8b9b7 !important;
		-webkit-box-shadow: 0 0 6px #f8b9b7 !important;
		-moz-box-shadow: 0 0 6px #f8b9b7 !important;
	}
</style>
<div class="container-fluid">
	<div class="wrap">
		<div class="header">
			<div class="pull-left">
				<ul class="breadcrumb">
                    <li><a href="<?php echo site_url('c_home'); ?>" class="icon-home"></a>
                    </li>
                    <li><a href="<?php echo site_url('c_home/password'); ?>">Change Password</a>
                    </li>
                </ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<?php echo form_open('c_home/password') ?>
		<div class="well">
			<fieldset>
				<legend>Change Password</legend>
				<div>
					<label>Old Password</label>
					<input type="password" value="" class="span12" required="" name="old_password" placeholder="Old Password">
				</div>
				<div>
					<label>New Password</label>
					<input type="password" value="" class="span12" required="" name="new_password" placeholder="New Password">
				</div>
				<div>
					<label>Re-type New Password</label>
					<input type="password" value="" class="span12" required="" name="re_password" placeholder="Re-type Password">
				</div>
				<div class="bottom-1">
					<input type="submit" />
					<a href="<?php echo site_url('c_home'); ?>" class="btn">Cancel</a>
				</div>
			</fieldset>
		</div>
	<?php echo form_close() ?>
</div>

<script type="text/javascript">
$(function() {
	$('input[type=submit]').click(function() {
		var bpass = $('input[name=new_password]');
		var apass = $('input[name=re_password]');

		bpass.focusout(function() {
			bpass.removeClass('error');
		});

		apass.focusout(function() {
			apass.removeClass('error');
		});

		if(bpass.val() != apass.val()) {
			alert("Terjadi kesalahan!");
			bpass.addClass('error');
			apass.addClass('error');
			bpass.focus();
			bpass.val(null);
			apass.val(null);
			return false;
		}
	})
});
</script>