<div class="container">
    <div class="row-fluid">
        <fieldset class="desktop">
            <legend>DATA</legend>
            <div class="row-fluid">
                <div class="span6 item">
                    <a href="<?php echo site_url ('c_siswa') ?>" title="">
                        <img src="<?php echo base_url('assets/img/default.png') ?>" width="150" height="150">
                        <div class="clear"></div>
                        <h6>Siswa</h6>
                    </a>
                </div>
                <div class="span6 item">
                    <a href="<?php echo site_url ('c_guru') ?>" title="">
                        <img src="<?php echo base_url('assets/img/work.png') ?>" width="150" height="150">
                        <div class="clear"></div>
                        <h6>Guru</h6>
                    </a>
                </div>
            </div>
        </fieldset>

        <fieldset class="desktop">
            <legend>SPP</legend>
            <div class="row-fluid">
                <div class="span4 item">
                    <a href="<?php echo site_url ('c_spp') ?>" title="">
                        <img src="<?php echo base_url('assets/img/spp.png') ?>" width="150" height="150">
                        <div class="clear"></div>
                        <h6>Spp</h6>
                    </a>
                </div>
                <div class="span4 item">
                    <a href="<?php echo site_url ('c_spp/payment') ?>" title="">
                        <img src="<?php echo base_url('assets/img/saveSpp.png') ?>" width="150" height="150">
                        <div class="clear"></div>
                        <h6>Pembayaran Spp</h6>
                    </a>
                </div>
                 <div class="span4 item">
                    <a href="<?php echo site_url ('c_spp/building') ?>" title="">
                        <img src="<?php echo base_url('assets/img/saveSpp.png') ?>" width="150" height="150">
                        <div class="clear"></div>
                        <h6>Pembayaran Bangunan dan lain-lain</h6>
                    </a>
                </div>
            </div>
        </fieldset>

        <fieldset class="desktop">
            <legend>Laporan</legend>
            <div class="row-fluid">
                <div class="span6 item">
                    <a href="<?php echo site_url ('c_reports/siswa') ?>" title="">
                        <img src="<?php echo base_url('assets/img/PDF-icon.png') ?>" width="120" height="120">
                        <div class="clear"></div>
                        <h6>Cetak Siswa</h6>
                    </a>
                </div>
                <div class="span6 item">
                    <a href="<?php echo site_url ('c_reports/trancation') ?>" title="">
                        <img src="<?php echo base_url('assets/img/PDF-icon.png') ?>" width="120" height="120">
                        <div class="clear"></div>
                        <h6>Cetak Transaksi</h6>
                    </a>
                </div>
            </div>
        </fieldset>
    </div>
  </div>    