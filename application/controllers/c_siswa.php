<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_siswa extends CI_Controller {

    public function session() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url('c_login'), 'refresh');
        }
    }

	public function index() {
		$this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_siswa/index");
        $config["total_rows"] = $this->m_database->count_siswa();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_database->fetch_siswa($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

    	// $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/siswa/v_siswa', $data);
        $this->load->view('template/v_footer', $data);
	}

    public function siswa() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/siswa/v_tambah_siswa', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function check_siswa() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $noInduk = explode(",",$data);
        $check = $this->m_database->check_siswa($noInduk);
        echo json_encode($check);
    }

    public function add_siswa() {
        $this->session();
        //result query
        $noInduk = $this->input->post('noInduk');
        $nama = $this->input->post('nama');
        $kelas = $this->input->post('kelas');
        $result = $this->m_database->add_siswa($noInduk, $nama, $kelas);

        $this->index();
        
        // xlog($noInduk);
        
        // $cnoInduk = count($noInduk);
        // if($cnoInduk) {
        //     for ($x=0; $x < $cnoInduk; $x++) { 
        //         $str[] =  "('{$noInduk[$x]}', '{$nama[$x]}', '{$kelas[$x]}')"; 
        //     }
        // }

        // xlog($str);
        // echo 'Jumlah values '.count($str).'<br />';
        // $s = implode(',',$str);
        // print_r($s);
        // exit();
        
        //if session is true, redirect to home view
        // $session_data = $this->session->userdata('logged_in');
        // $data['username'] = $session_data['username'];
        // $this->m_database->add_siswa($noInduk, $nama, $kelas);
    }

    public function delete() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('delete');
        $delete = $this->m_database->delete_siswa($noInduk);
        header('location:'.site_url('c_siswa').'');
    }

    public function show_edit() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('edit');
        $data['hslquery'] = $this->m_database->show_edit($noInduk);

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/siswa/v_edit', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function edit() {
        //check session
        $this->session();
         //validate form
        $this->form_validation->set_rules('noInduk','noInduk','trim|required|max_length[10]|regex_match[/^\d+$/]');
        $this->form_validation->set_rules('nama', 'Nama','trim|required|regex_match[/^([^\d]+)$/]|xss_clean');
        $this->form_validation->set_rules('kelas', 'kelas','trim|required|max_length[5]|regex_match[/^([^\d]+)$/]|xss_clean');

        if($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->siswa();
        } else {
            //result query
            $noInduk = $this->input->post('noInduk');
            $nama = $this->input->post('nama');
            $kelas = $this->input->post('kelas');
            $result = $this->m_database->edit_siswa($noInduk, $nama, $kelas);
            header('location:'.site_url('c_siswa').'');
        }
    }

    public function search() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_siswa/index");
        $config["total_rows"] = $this->m_database->count_siswa();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $key = $this->input->post('search');
        if(preg_match('/^\d+$/', $key)) {
           $data["hslquery"] = $this->m_database->search_siswa('int', $key);
        } else {
            $data["hslquery"] = $this->m_database->search_siswa('str', $key);
        }
        
        $data["paginator"] = $this->pagination->create_links();

        // $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/siswa/v_siswa', $data);
        $this->load->view('template/v_footer', $data);

    }

    public function trash() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_database->trash($data_array);
        header('location:'.site_url('c_siswa').'');
    }

    public function lulus() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data = $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_database->lulus($data_array);
        header('location:'.site_url('c_siswa').'');
    }
}

/* End of file c_siswa.php */
/* Location: ./application/controllers/c_siswa.php */
