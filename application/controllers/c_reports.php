<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_reports extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array("m_reports","m_spp"));
	}

	public function session() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url('c_login'), 'refresh');
        }
    }

    public function siswa() {
    	$this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("C_reports/index");
        $config["total_rows"] = $this->m_database->count_siswa();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_database->fetch_siswa($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

    	// $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('reports/v_siswa', $data);
        $this->load->view('template/v_footer', $data);
    }

	public function siswa_print_all() {	
		$this->session();
        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];
        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $data["hslquery"] = $this->m_reports->siswa();
        // xlog($data["hslquery"]);
        // exit();

		// Load all views as normal
		$this->load->view('reports/v_siswa_pdf',$data);
		// // // exit();
		// Get output html
		$html = $this->output->get_output();
		
		// Load library
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		
		$this->dompdf->stream("Semua_Siswa.pdf",array('Attachment'=>0));
		
	}

    public function siswa_kelas() {
        $this->session();
        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];
        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $kelas = $this->input->get('kelas');
        $nim = $this->input->get('nim');
        $data["hslquery"] = $this->m_reports->siswa_kelas($kelas, $nim);
        // xlog($data["hslquery"]);
        // exit();

        // Load all views as normal
        $this->load->view('reports/v_siswa_pdf',$data);
        // // // exit();
        // Get output html
        $html = $this->output->get_output();
        
        // Load library
        $this->load->library('dompdf_gen');
        
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        
        $this->dompdf->stream("Siswa_Perkelas.pdf",array('Attachment'=>0));
    }

    public function trancation() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_reports/trancation");
        $config["total_rows"] = $this->m_spp->count_payment();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_spp->fetch_payment($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

        // $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('reports/v_transaction', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function transaction_print() {
        $this->session();
        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];
        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $data["hslquery"] = $this->m_reports->transaction();

        // Load all views as normal
        $this->load->view('reports/v_transaction_pdf',$data);
        // // // exit();
        // Get output html
        $html = $this->output->get_output();
        
        // Load library
        $this->load->library('dompdf_gen');
        
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        
        $this->dompdf->stream("Transaksi.pdf",array('Attachment'=>0));
        
        
    }
}
/* End of file c_spp.php */
/* Location: ./application/controllers/c_spp.php */