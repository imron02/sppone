<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_spp extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("m_spp");
	}

	public function session() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url('c_login'), 'refresh');
        }
    }

	public function index() {
		$this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $this->config->item('meta');

        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data["hslquery"] = $this->m_spp->spp();
        $data['instansi'] = $this->config->item('nama_instansi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_spp', $data);
        $this->load->view('template/v_footer', $data);
	}

	public function spp() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_tambah_spp', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function add_spp() {
        $this->session();
        $data = array(
            'kelas' => $_POST['kelas'],
            'spp' => $_POST['spp'],
            'bangunan' => $_POST['bangunan'],
            'lain_lain' => $_POST['lain-lain']
            );
        $result = $this->m_spp->add_spp($data);

        $this->index();
    }

    public function show_edit() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        if($_POST) {
            $data = array(
                'kelas' => $_POST['kelas'],
                'spp' => $_POST['spp'],
                'bangunan' => $_POST['bangunan'],
                'lain_lain' => $_POST['lain_lain']
                );
            $this->m_spp->spp_edit($data);
            redirect('/c_spp/');
        }

        $kelas = $this->input->get('edit');
        $data['hslquery'] = $this->m_spp->show_edit($kelas);

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_edit', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function check_spp() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $kelas = explode(",",$data);
        $check = $this->m_spp->check_spp($kelas);
        echo json_encode($check);
    }

    public function delete() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $kelas = $this->input->get('delete');
        $delete = $this->m_spp->delete_spp($kelas);
        header('location:'.site_url('c_spp'));
    }

    public function search() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        
        $key = $this->input->post('search');
        $data["hslquery"] = $this->m_spp->search_spp($key);

        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_spp', $data);
        $this->load->view('template/v_footer', $data);

    }

    public function trash() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_spp->trash($data_array);
        header('location:'.site_url('c_spp').'');
    }

    public function payment() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_spp/payment");
        $config["total_rows"] = $this->m_spp->count_payment();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_spp->fetch_payment($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

        // $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_tunggakan', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function payment_search() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_spp/payment");
        $config["total_rows"] = $this->m_spp->count_payment();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $key = $this->input->post('search');
        if(preg_match('/^\d+$/', $key)) {
           $data["hslquery"] = $this->m_spp->search_payment('int', $key);
        } else {
            $data["hslquery"] = $this->m_spp->search_payment('str', $key);
        }
        
        $data["paginator"] = $this->pagination->create_links();
        
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_tunggakan', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function payment_show_edit() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('edit');
        $data['hslquery'] = $this->m_spp->payment_show_edit($noInduk);
        $data['hslquery1'] = $this->m_spp->payment_tunggakan($noInduk);
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_payment_edit', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function payment_show_add() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $data['bayar'] = $this->input->get('bayar');
        $data['bulan'] = $this->input->get('bulan');
        $data['perbulan'] = $this->input->get('perbulan');
        $check_bulan = $this->m_spp->mounth_check($data['bayar']);
        for ($i=0; $i < count($check_bulan) ; $i++) { 
            $bulan2[$check_bulan[$i]['bulan']] = $check_bulan[$i]['bulan'];
        }
        $bulan1 = array('' => '',
            'Januari' => 'Januari',
            'Februari' => 'Februari',
            'Maret' => 'Maret',
            'April' => 'April',
            'Mei' => 'Mei',
            'Juni' => 'Juni',
            'Juli' => 'Juli',
            'Agustus' => 'Agustus',
            'September' => 'September',
            'Oktober' => 'Oktober',
            'November' => 'November',
            'Desember' => 'Desember'
            );
        $data['options'] = array_diff_assoc($bulan1, $bulan2);
        array_unshift($data['options'], '-Pilih-');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_payment_add', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function payment_add() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $arr = $this->input->post();
        $noInduk = $arr['noInduk'];
        $perbulan = $arr['perbulan'];
        $month = array_slice($arr,2);
        $data['hslquery'] = $this->m_spp->payment_add($noInduk, $perbulan, $month);
        header('location:'.site_url('c_spp/payment_show_edit').'?edit='.$noInduk);
    }

    public function payment_delete() {
        $this->session();
        $noInduk = $this->input->get('edit');
        $id = $this->input->get('id');

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $delete = $this->m_spp->payment_delete($id);

        header('location:'.site_url('c_spp/payment_show_edit').'?edit='.$noInduk);
    }

    public function payment_show_update() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data['update'] = $this->input->get('update');
        $data['noInduk'] = $this->input->get('noInduk');
        
        $check_bulan = $this->m_spp->mounth_check($data['noInduk']);
        for ($i=0; $i < count($check_bulan) ; $i++) { 
            $bulan2[$check_bulan[$i]['bulan']] = $check_bulan[$i]['bulan'];
        }
        $bulan1 = array('' => '',
            'Januari' => 'Januari',
            'Februari' => 'Februari',
            'Maret' => 'Maret',
            'April' => 'April',
            'Mei' => 'Mei',
            'Juni' => 'Juni',
            'Juli' => 'Juli',
            'Agustus' => 'Agustus',
            'September' => 'September',
            'Oktober' => 'Oktober',
            'November' => 'November',
            'Desember' => 'Desember'
            );
        $data['options'] = array_diff_assoc($bulan1, $bulan2);
        array_unshift($data['options'], '-Pilih-');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_payment_update', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function payment_update() {
        $this->session();
        $month = $this->input->post('month');
        $update = $this->input->post('update');
        $noInduk = $this->input->post('noInduk');
        $data['hslquery'] = $this->m_spp->payment_update($update, $month);
        header('location:'.site_url('c_spp/payment_show_edit').'?edit='.$noInduk);
    }

    public function payment_trash() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data= $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_spp->payment_trash($data_array);
    }

    public function building() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_spp/building");
        $config["total_rows"] = $this->m_spp->count_building();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_spp->fetch_building($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

        // $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_building', $data);
        $this->load->view('template/v_footer', $data);   
    }

    public function building_search() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_spp/building");
        $config["total_rows"] = $this->m_spp->count_building();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $key = $this->input->post('search');
        if(preg_match('/^\d+$/', $key)) {
           $data["hslquery"] = $this->m_spp->search_building('int', $key);
        } else {
            $data["hslquery"] = $this->m_spp->search_building('str', $key);
        }
        
        $data["paginator"] = $this->pagination->create_links();
        
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_building', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function building_show_edit() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('edit');
        $data['hslquery'] = $this->m_spp->building_show_edit($noInduk);
        $data['hslquery1'] = $this->m_spp->building_tunggakan($noInduk);
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_building_edit', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function building_payment() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data['no_induk'] = $this->input->get('data');

        if($_POST) {
            $data = array(
                'bangunan' => $_POST['bangunan'],
                'lain_lain' => $_POST['lain_lain'],
                'no_induk' => $_POST['no_induk']
                );
            $this->m_spp->building_payment($data);
            redirect('/c_spp/building_show_edit?edit=' . $_POST['no_induk']);
        }

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_building_payment', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function building_show_update() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $update = $this->input->get('update');
        $data['noInduk'] = $this->input->get('noInduk');

        $data['hslquery'] = $this->m_spp->show_building_update($update, $data['noInduk']);

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/spp/v_building_update', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function building_update() {
        $this->session();
        $noInduk = $this->input->post('noInduk');
        $data = array(
            'no_transaksi' => $_POST['noTransaksi'],
            'bangunan' => $_POST['bangunan'],
            'lain_lain' => $_POST['lain_lain'] 
            );
        $this->m_spp->building_update($data);
        redirect('/c_spp/building_show_edit?edit=' . $noInduk);
    }

    public function building_delete() {
        $this->session();
        $noInduk = $this->input->get('edit');
        $id = $this->input->get('id');

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $delete = $this->m_spp->building_delete($id);

        redirect('/c_spp/building_show_edit?edit=' . $noInduk);
    }

    public function building_trash() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data= $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_spp->payment_trash($data_array);
    }
}
/* End of file c_spp.php */
/* Location: ./application/controllers/c_spp.php */