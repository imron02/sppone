<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library("pagination");
    }

    public function index() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url("c_login?continue=".$this->input->get('continue')), 'refresh');
        }
        //if session is true, redirect to home view
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            // $data['id'] = $session_data['no'];

            $data['meta'] = array(
    		array('name' => 'robots', 'content' => 'no-cache'),
    		array('name' => 'description', 'content' => 'Login Spp'),
        	array('name' => 'keywords', 'content' => 'pembayaran spp, spp, darul hikmah'),
        	array('name' => 'robots', 'content' => 'no-cache'),
        	array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
        	);

        	$data['judul_pendek'] = $this->config->item('nama_aplikasi');
            $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $data['instansi'] = $this->config->item('nama_instansi');
            $data['alamat'] = $this->config->item('alamat_instansi');
            $data['credit'] = $this->config->item('credit_aplikasi');
            $this->load->view('template/v_navbar', $data);
            $this->load->view('admin/home/v_dashboard', $data);
            $this->load->view('template/v_footer', $data);
     }

     public function password() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url("c_login?continue=".$this->input->get('continue')), 'refresh');
        }

        //if session is true, redirect to home view
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            // $data['id'] = $session_data['no'];

            $data['meta'] = array(
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'description', 'content' => 'Login Spp'),
            array('name' => 'keywords', 'content' => 'pembayaran spp, spp, darul hikmah'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
            );

            if($_POST) {
                $data = array(
                    'old_password' =>  $_POST['old_password'],
                    'new_password' => $_POST['new_password'],
                    're_password' => $_POST['re_password'],
                    'username' => $data['username']
                    );
                $this->m_database->password($data);
                redirect('/c_home');
            }

            $data['judul_pendek'] = $this->config->item('nama_aplikasi');
            $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
            $data['instansi'] = $this->config->item('nama_instansi');
            $data['alamat'] = $this->config->item('alamat_instansi');
            $data['credit'] = $this->config->item('credit_aplikasi');
            $this->load->view('template/v_navbar', $data);
            $this->load->view('admin/home/v_password', $data);
            $this->load->view('template/v_footer', $data);
     }

     public function logout() {
         //remove all session data
         $this->session->unset_userdata('logged_in');
         $this->session->sess_destroy();
         // redirect($this->input->get('last_url'));
         // print_r($this->input->get('last_url'));
         redirect(site_url("c_login?continue=".$this->input->get('continue')), 'refresh');
     }
    
}
/* End of file c_home.php */
/* Location: ./application/controllers/c_home.php */
