<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_login extends CI_Controller {

    public function index() {
        if($this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url('c_home'),'refresh');
        }
        $data = array();
    	$data['meta'] = array(
    		array('name' => 'robots', 'content' => 'no-cache'),
    		array('name' => 'description', 'content' => 'Login Spp'),
        	array('name' => 'keywords', 'content' => 'pembayaran spp, spp, darul hikmah'),
        	array('name' => 'robots', 'content' => 'no-cache'),
        	array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
        	);
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi');
        $data['instansi'] = $this->config->item('nama_instansi');
        $data['credit'] = $this->config->item('credit_aplikasi');
        $data['alamat'] = $this->config->item('alamat_instansi');
        $this->load->view('app/v_login',$data); //load view for login
    }

    public function login() {
        // print_r($this->input->post('continue'));
        // exit();
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $data['meta'] = array(
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'description', 'content' => 'Login Spp'),
            array('name' => 'keywords', 'content' => 'pembayaran spp, spp, darul hikmah'),
            array('name' => 'robots', 'content' => 'no-cache'),
            array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
            );
            $data['judul_lengkap'] = $this->config->item('nama_aplikasi');
            $data['instansi'] = $this->config->item('nama_instansi');
            $data['credit'] = $this->config->item('credit_aplikasi');
            $data['alamat'] = $this->config->item('alamat_instansi');
            $this->load->view('app/v_login',$data);
            } else {
                $url = $this->input->post('continue') ? $this->input->post('continue') : 'c_home';

                redirect($url, 'refresh');
            }       
     }

     public function check_database($password) {
         //Field validation succeeded.  Validate against database
         $username = $this->input->post('username');
         //query the database
         $result = $this->m_database->login($username, $password);
         if($result) {
             $sess_array = array();
             $sess_array = array('username' => $result['username']);
             $this->session->set_userdata('logged_in', $sess_array);
          return TRUE;
          } else {
              //if form validate false
              $this->form_validation->set_message('check_database', 'Invalid username or password');
              return FALSE;
          }
      }
}
/* End of file c_login.php */
/* Location: ./application/controllers/c_login.php */