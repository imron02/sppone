<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class C_guru extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model("m_guru");
    }

	 public function session() {
        if(!$this->session->userdata('logged_in')) {
        //If no session, redirect to login page
            redirect(site_url('c_login'), 'refresh');
        }
    }

    public function index() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_guru/index");
        $config["total_rows"] = $this->m_guru->count_guru();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["hslquery"] = $this->m_guru->fetch_guru($config["per_page"], $page);
        $data["paginator"] = $this->pagination->create_links();

        // $data['judul_pendek'] = $this->config->item('nama_aplikasi');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        //    $data['alamat'] = $this->config->item('alamat_instansi');
        //    $data['credit'] = $this->config->item('credit_aplikasi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/guru/v_guru', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function guru() {
        $this->session();

        $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/guru/v_tambah_guru', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function check_guru() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $noInduk = explode(",",$data);
        $check = $this->m_guru->check_guru($noInduk);
        echo json_encode($check);
    }

    public function add_guru() {
        $this->session();
        // xlog($_POST);
        // exit();
    
        // $noInduk = $this->input->post('noInduk');
        // $nama = $this->input->post('nama');
        // $ttl = $this->input->post('ttl');
        // $jk = $this->input->post('jk');
        // $ijazah = $this->input->post('ijazah');
        // $jabatan = $this->input->post('jabatan');
        // $mengajar = $this->input->post('mengajar');
        // $bidang_studi = $this->input->post('bidang_studi');
        // $tlp = $this->input->post('tlp');

        $data = array(
            'noInduk' => $noInduk = $this->input->post('noInduk'),
            'nama' => $this->input->post('nama'),
            'ttl' => $this->input->post('ttl'),
            'jk' => $this->input->post('jk'),
            'ijazah' => $this->input->post('ijazah'),
            'jabatan' => $this->input->post('jabatan'),
            'mengajar' => $this->input->post('mengajar'),
            'bidang_studi' => $this->input->post('bidang_studi'),
            'tlp' => $this->input->post('tlp')
            );
        // $result = $this->m_guru->add_guru($noInduk, $nama, $ttl, $jk, $ijazah, $jabatan, $mengajar, $bidang_studi, $tlp);
        $result = $this->m_guru->add_guru($data);
        $this->guru();
    }

    public function delete() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('delete');
        $delete = $this->m_guru->delete_guru($noInduk);
        header('location:'.site_url('c_guru').'');
    }

    public function show_edit() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $noInduk = $this->input->get('edit');
        $data['hslquery'] = $this->m_guru->show_edit($noInduk);

        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/guru/v_edit', $data);
        $this->load->view('template/v_footer', $data);
    }

    public function edit() {
        //check session
        $this->session();
        // $noInduk = $this->input->post('noInduk');
        //     $nama = $this->input->post('nama');
        //     $tlp = $this->input->post('tlp');
        //     $alamat = $this->input->post('alamat');
        //  //validate form
        // xlog($_POST);
        // exit();
        $this->form_validation->set_rules('noInduk','noInduk','trim|required|max_length[10]|regex_match[/^\d+$/]');
        $this->form_validation->set_rules('nama', 'Nama','trim|required|regex_match[/^([^\d]+)$/]|xss_clean');
        $this->form_validation->set_rules('tlp', 'tlp','trim|required|min_length[7]|max_length[13]|regex_match[/^\d+$/]|xss_clean');
        $this->form_validation->set_rules('alamat', 'alamat','trim|required|xss_clean');

        if($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->guru();
        } else {
            //result query
            $noInduk = $this->input->post('noInduk');
            $nama = $this->input->post('nama');
            $tlp = $this->input->post('tlp');
            $alamat = $this->input->post('alamat');
            $result = $this->m_guru->edit_guru($noInduk, $nama, $tlp, $alamat);
            header('location:'.site_url('c_guru').'');
        }
    }

    public function search() {
        $this->session();

        //if session is true, redirect to home view
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        // $data['id'] = $session_data['no'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');

        $config = array();
        $config["base_url"] = site_url("c_guru/index");
        $config["total_rows"] = $this->m_guru->count_guru();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $key = $this->input->post('search');
        if(preg_match('/^\d+$/', $key)) {
           $data["hslquery"] = $this->m_guru->search_guru('int', $key);
        } else {
            $data["hslquery"] = $this->m_guru->search_guru('str', $key);
        }
        
        $data["paginator"] = $this->pagination->create_links();

        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');
        $this->load->view('template/v_navbar', $data);
        $this->load->view('admin/guru/v_guru', $data);
        $this->load->view('template/v_footer', $data);

    }

    public function trash() {
        $this->session();

        //if session is true
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $data['meta'] = $data['meta'] = $this->config->item('meta');
        $data['judul_lengkap'] = $this->config->item('nama_aplikasi_full');
        $data['instansi'] = $this->config->item('nama_instansi');

        $data= $this->input->post('data');
        $data_array = explode(",",$data);
        $delete = $this->m_guru->trash($data_array);
        header('location:'.site_url('c_guru').'');
    }

}
/* End of file c_guru.php */
/* Location: ./application/controllers/c_guru.php */
