<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class M_spp extends CI_Model
{
	function spp() {
		$sql = "SELECT kelas,spp,(spp/12)::int AS perbulan,bangunan,lain_lain FROM spp";
	    $q = $this->db->conn_id->prepare($sql);
	    $q->execute();
	    return $result = $q->fetchAll(PDO::FETCH_ASSOC);
	}

    function show_edit($kelas) {
        $sql = "SELECT * FROM spp WHERE kelas = :kelas";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":kelas", $kelas, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetch(PDO::FETCH_ASSOC);
    }

    function spp_edit($data) {
        $sql = "UPDATE spp SET spp = :spp, bangunan = :bangunan, lain_lain = :lain_lain WHERE kelas = :kelas";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':spp', $data['spp'], PDO::PARAM_INT);
        $q->bindParam(':bangunan', $data['bangunan'], PDO::PARAM_INT);
        $q->bindParam(':lain_lain', $data['lain_lain'], PDO::PARAM_INT);
        $q->bindParam(':kelas', $data['kelas'], PDO::PARAM_STR);
        return $q->execute();
    }

	function add_spp($data) {
        $ckelas = count($data['kelas']);
        
        //get count values
        $rValue = ($ckelas  ? $ckelas - 1 : 0);
        $cValue = "(?,?,?,?)".str_repeat(",(?,?,?,?)", $rValue);
        
        $sql = "INSERT INTO spp (kelas,spp,bangunan,lain_lain) VALUES $cValue";
        $q = $this->db->conn_id->prepare($sql);

        //function binding

        $a = 1; $b = 2; $c = 3; $d = 4;
        for($i=0; $i < $ckelas; $i++) {
            $q->bindValue($a, $data['kelas'][$i], PDO::PARAM_STR);
            $q->bindValue($b, $data['spp'][$i], PDO::PARAM_INT);
            $q->bindValue($c, $data['bangunan'][$i], PDO::PARAM_INT);
            $q->bindValue($d, $data['lain_lain'][$i], PDO::PARAM_INT);
            $a+=4; $b+=4; $c+=4; $d+=4;
        }
        $q->execute();
    }

    function check_spp($kelas) {
        $ckelas = count($kelas);
        for($i = 0; $i < $ckelas; $i++) {
            $sql = "SELECT kelas FROM spp WHERE kelas = :kelas";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(':kelas', $kelas[$i], PDO::PARAM_INT);
            $q->execute();
            if($q->rowCount() > 0) {
                // if the key already exists then add it to the 
                // error array
                $kelasError[] = $kelas[$i];
            }
        }
        if (!empty($kelasError)){
            return $kelasError;
        }

        return FALSE;
    }

    function delete_spp($kelas) {
        $sql = "DELETE FROM spp WHERE kelas = :kelas";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":kelas", $kelas, PDO::PARAM_STR);
        $delete = $q->execute();
        return $delete;
    }

     function search_spp($key) {
        $sql = "SELECT kelas,spp,(spp/12)::int AS perbulan,bangunan,lain_lain FROM spp WHERE kelas LIKE ?";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindValue(1, "%$key%", PDO::PARAM_STR);
        $q->execute();
        return $q->fetchAll(PDO::FETCH_ASSOC);
    }

    function trash($kelas) {
        $ckelas = count($kelas);
        for ($i=0; $i < $ckelas ; $i++) { 
            $sql = "DELETE FROM spp WHERE kelas = :kelas";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":kelas", $kelas[$i], PDO::PARAM_STR);
            $delete = $q->execute();
        }
        return $delete;
    }

    function count_payment() {
        $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.spp, sum(transaksi_spp.dibayar) AS dibayar, 
        spp.spp - sum(transaksi_spp.dibayar) AS tunggakan, transaksi_spp.bulan FROM siswa, spp, transaksi_spp 
        WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk = transaksi_spp.no_induk GROUP BY siswa.no_induk, 
        spp.spp, transaksi_spp.bulan ORDER BY siswa.no_induk, siswa.kelas";
        $q = $this->db->conn_id->prepare($sql);
        $q->execute();
        return $q->rowCount();
    }

    function fetch_payment($limit, $start) {
        $sql = "SELECT siswa.no_induk,siswa.nama,siswa.kelas,siswa.kelas,spp.spp,sum(transaksi_spp.dibayar)AS dibayar,
        spp.spp - sum(transaksi_spp.dibayar)AS tunggakan FROM siswa,spp,transaksi_spp WHERE siswa.kelas = spp.kelas AND 
        siswa.no_induk = transaksi_spp.no_induk GROUP BY siswa.no_induk,spp.spp LIMIT :limit OFFSET :start";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':limit', $limit, PDO::PARAM_INT);
        $q->bindParam(':start', $start, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);
    }

    function search_payment($id, $key) {
        if($id == "int") {
            $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.spp, sum(transaksi_spp.dibayar) AS 
            dibayar, spp.spp - sum(transaksi_spp.dibayar) AS tunggakan, transaksi_spp.bulan FROM siswa, spp, 
            transaksi_spp WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk::text 
            LIKE ? AND transaksi_spp.no_induk::text LIKE ? GROUP BY siswa.no_induk, spp.spp, transaksi_spp.bulan ORDER BY 
            siswa.no_induk, siswa.kelas";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_INT);
            $q->bindValue(2, "%$key%", PDO::PARAM_INT);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.spp, sum(transaksi_spp.dibayar) AS dibayar, 
            spp.spp - sum(transaksi_spp.dibayar) AS tunggakan, transaksi_spp.bulan FROM siswa, spp, transaksi_spp 
            WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk = transaksi_spp.no_induk AND siswa.nama 
            LIKE ? GROUP BY siswa.no_induk, spp.spp, transaksi_spp.bulan ORDER BY siswa.no_induk, siswa.kelas;";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_STR);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    function payment_show_edit($noInduk) {
        $sql = "SELECT transaksi_spp.no_transaksi, transaksi_spp.no_induk, siswa.nama, siswa.kelas, transaksi_spp.tanggal, 
        transaksi_spp.dibayar, transaksi_spp.bulan FROM siswa, transaksi_spp WHERE siswa.no_induk = :noInduk AND  
        transaksi_spp.no_induk = :noInduk AND transaksi_spp.dibayar IS NOT NULL ORDER BY transaksi_spp.no_transaksi";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);
    }

    function payment_tunggakan($noInduk) {
        $sql = "SELECT transaksi_spp.no_induk,(spp.spp/12::real)AS perbulan, spp.spp - sum(transaksi_spp.dibayar)AS tunggakan
         FROM spp, transaksi_spp,siswa WHERE spp.kelas = siswa.kelas AND siswa.no_induk = :noInduk AND 
         transaksi_spp.no_induk = :noInduk GROUP BY spp.spp, transaksi_spp.no_induk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetch(PDO::FETCH_ASSOC);
    }

    function mounth_check($noInduk) {
        $sql = "SELECT no_induk, bulan from transaksi_spp WHERE no_induk = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $q->fetchAll();
    }

    function payment_add($noInduk, $perbulan, $month) {
        $tgl = date("Y-m-d");
        $cmonth = count($month);
        
        //get count values
        $rValue = ($cmonth  ? $cmonth - 1 : 0);
        $cValue = "(?,?,?,?)".str_repeat(",(?,?,?,?)", $rValue);
        
        $sql = "INSERT INTO transaksi_spp (bulan,no_induk,tanggal,dibayar) VALUES $cValue";
        $q = $this->db->conn_id->prepare($sql);

        //function binding

        $a = 1; $b = 2; $c = 3; $d = 4;
        for($i=0; $i < $cmonth; $i++) {
            $q->bindValue($a, $month['month'.$i], PDO::PARAM_STR);
            $q->bindValue($b, $noInduk, PDO::PARAM_INT);
            $q->bindValue($c, $tgl, PDO::PARAM_STR);
            $q->bindValue($d, $perbulan, PDO::PARAM_INT);
            $a+=4; $b+=4; $c+=4; $d+=4;
        }
        $q->execute();
    }

    function payment_delete($id) {
        $sql = "DELETE FROM transaksi_spp WHERE no_transaksi = :id";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":id", $id, PDO::PARAM_INT);
        $delete = $q->execute();
    }

    function payment_update($id, $month) {
        $sql = "UPDATE transaksi_spp SET bulan = :month WHERE no_transaksi = :id";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":month", $month, PDO::PARAM_STR);
        $q->bindParam(":id", $id, PDO::PARAM_INT);
        $delete = $q->execute();        
    }

    function payment_trash($noTransaksi) {
        $cnoTransaksi = count($noTransaksi);
        for ($i=0; $i < $cnoTransaksi ; $i++) { 
            $sql = "DELETE FROM transaksi_spp WHERE no_transaksi = :noTransaksi";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":noTransaksi", $noTransaksi[$i], PDO::PARAM_INT);
            $delete = $q->execute();
        }
        return $delete;
    }

    function count_building() {
        $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.bangunan, sum(transaksi_spp.bangunan)AS bayar_bangunan, 
        spp.bangunan - sum(transaksi_spp.bangunan)AS tunggakan_bangunan, spp.lain_lain, 
        sum(transaksi_spp.lain_lain)AS bayar_lain, spp.lain_lain - sum(transaksi_spp.lain_lain)As tunggakan_lain 
        FROM siswa, spp, transaksi_spp WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk = transaksi_spp.no_induk 
        GROUP BY siswa.no_induk, spp.bangunan, spp.lain_lain ORDER BY siswa.no_induk, siswa.kelas";
        $q = $this->db->conn_id->prepare($sql);
        $q->execute();
        return $q->rowCount();
    }

    function fetch_building($limit, $start) {
        $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.bangunan, sum(transaksi_spp.bangunan)AS bayar_bangunan, 
        spp.bangunan - sum(transaksi_spp.bangunan)AS tunggakan_bangunan, spp.lain_lain, 
        sum(transaksi_spp.lain_lain)AS bayar_lain, spp.lain_lain - sum(transaksi_spp.lain_lain)As tunggakan_lain 
        FROM siswa, spp, transaksi_spp WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk = transaksi_spp.no_induk 
        GROUP BY siswa.no_induk, spp.bangunan, spp.lain_lain LIMIT :limit OFFSET :start";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':limit', $limit, PDO::PARAM_INT);
        $q->bindParam(':start', $start, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);
    }

    function search_building($id, $key) {
        if($id == "int") {
            $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.bangunan, sum(transaksi_spp.bangunan)AS bayar_bangunan, 
            spp.bangunan - sum(transaksi_spp.bangunan)AS tunggakan_bangunan, spp.lain_lain, 
            sum(transaksi_spp.lain_lain)AS bayar_lain, spp.lain_lain - sum(transaksi_spp.lain_lain)As tunggakan_lain 
            FROM siswa, spp, transaksi_spp WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk::text 
            LIKE ? AND transaksi_spp.no_induk::text LIKE ? GROUP BY siswa.no_induk, spp.bangunan, spp.lain_lain ORDER BY 
            siswa.no_induk, siswa.kelas";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_INT);
            $q->bindValue(2, "%$key%", PDO::PARAM_INT);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $sql = "SELECT siswa.no_induk, siswa.nama, siswa.kelas, spp.bangunan, sum(transaksi_spp.bangunan)AS bayar_bangunan, 
            spp.bangunan - sum(transaksi_spp.bangunan)AS tunggakan_bangunan, spp.lain_lain, 
            sum(transaksi_spp.lain_lain)AS bayar_lain, spp.lain_lain - sum(transaksi_spp.lain_lain)As tunggakan_lain 
            FROM siswa, spp, transaksi_spp 
            WHERE spp.kelas::text = siswa.kelas::text AND siswa.no_induk = transaksi_spp.no_induk AND siswa.nama 
            LIKE ? GROUP BY siswa.no_induk, spp.bangunan, spp.lain_lain ORDER BY siswa.no_induk, siswa.kelas;";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_STR);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    function building_show_edit($noInduk) {
        $sql = "SELECT transaksi_spp.no_transaksi, transaksi_spp.no_induk, siswa.nama, siswa.kelas, transaksi_spp.tanggal, transaksi_spp.bangunan, 
        transaksi_spp.lain_lain FROM siswa, spp, transaksi_spp WHERE spp.kelas::text = siswa.kelas::text  AND siswa.no_induk = :noInduk 
        AND transaksi_spp.no_induk = :noInduk AND transaksi_spp.bangunan IS NOT NULL AND transaksi_spp.lain_lain IS NOT NULL
        GROUP BY transaksi_spp.no_transaksi, siswa.nama, siswa.kelas, spp.bangunan, spp.lain_lain";

        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);
    }

    function building_tunggakan($noInduk) {
        $sql = "SELECT  transaksi_spp.no_induk, spp.bangunan - sum(transaksi_spp.bangunan)AS tunggakan_bangunan, 
        spp.lain_lain - sum(transaksi_spp.lain_lain)AS tunggakan_lain FROM siswa, spp, transaksi_spp WHERE spp.kelas = siswa.kelas 
        AND siswa.no_induk = :noInduk AND transaksi_spp.no_induk = :noInduk GROUP BY spp.spp, transaksi_spp.no_induk, spp.bangunan, spp.lain_lain";

        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);   
    }

    function building_payment($data) {
        $tgl = date("Y-m-d");
        
        $tgl = date("Y-m-d");
        $sql = "INSERT INTO transaksi_spp (no_induk,tanggal,bangunan,lain_lain) VALUES (?,?,?,?)";
        $q = $this->db->conn_id->prepare($sql);

        $q->bindValue(1, $data['no_induk'], PDO::PARAM_INT);
        $q->bindValue(2, $tgl, PDO::PARAM_STR);
        $q->bindValue(3, $data['bangunan'], PDO::PARAM_INT);
        $q->bindValue(4, $data['lain_lain'], PDO::PARAM_INT);

        $q->execute();
    }

    function show_building_update($update, $noInduk) {
        $sql = " SELECT no_transaksi,no_induk,bangunan,lain_lain FROM transaksi_spp WHERE no_transaksi = :update";
        $q = $this->db->conn_id->prepare($sql);

        $q->bindParam(":update", $update, PDO::PARAM_INT);
        $q->execute();
        return $q->fetch(PDO::FETCH_ASSOC);
    }

    function building_update($data) {
        $sql = "UPDATE transaksi_spp SET bangunan = :bangunan, lain_lain = :lain_lain WHERE no_transaksi = :id";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":bangunan", $data['bangunan'], PDO::PARAM_INT);
        $q->bindParam(":lain_lain", $data['lain_lain'], PDO::PARAM_INT);
        $q->bindParam(":id", $data['no_transaksi'], PDO::PARAM_INT);
        $delete = $q->execute(); 
    }

    function building_delete($id) {
        $sql = "DELETE FROM transaksi_spp WHERE no_transaksi = :id";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":id", $id, PDO::PARAM_INT);
        $delete = $q->execute();
    }

    function building_trash($noTransaksi) {
        $cnoTransaksi = count($noTransaksi);
        for ($i=0; $i < $cnoTransaksi ; $i++) { 
            $sql = "DELETE FROM transaksi_spp WHERE no_transaksi = :noTransaksi";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":noTransaksi", $noTransaksi[$i], PDO::PARAM_INT);
            $delete = $q->execute();
        }
        return $delete;
    }
}
/* End of file m_spp.php */
/* Location: ./application/models/m_spp.php */