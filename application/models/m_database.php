<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_database extends CI_Model
{

	public function login($username, $password) {

        $sql = "SELECT * FROM admin WHERE username = :username AND password = md5(:password)";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':username', $username, PDO::PARAM_STR);
        $q->bindParam(':password', $password);
        $q->execute();

        $result = $q->rowCount();
        if ($result > 0) {
            return $q->fetch(PDO::FETCH_ASSOC);
        } else {
            return FALSE;
        }
	}

	public function count_siswa() {
        $sql = "SELECT * FROM siswa";
        $q = $this->db->conn_id->prepare($sql);
        $q->execute();
        return $q->rowCount();
    }

    public function fetch_siswa($limit, $start) {
        $sql = "SELECT * FROM siswa ORDER BY no_induk LIMIT :limit OFFSET :start";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':limit', $limit, PDO::PARAM_INT);
        $q->bindParam(':start', $start, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll(PDO::FETCH_ASSOC);
    }

    public function add_siswa($noInduk, $nama, $kelas) {
        $cnoInduk = count($noInduk);
        
        //get count values
        $rValue = ($cnoInduk  ? $cnoInduk - 1 : 0);
        $cValue = "(?,?,?)".str_repeat(",(?,?,?)", $rValue);
        
        $sql = "INSERT INTO siswa (no_induk,nama,kelas) VALUES $cValue";
        $q = $this->db->conn_id->prepare($sql);

        //function binding

        $a = 1; $b = 2; $c = 3;
        for($i=0; $i < $cnoInduk; $i++) {
            $q->bindValue($a, $noInduk[$i], PDO::PARAM_INT);
            $q->bindValue($b, $nama[$i], PDO::PARAM_STR);
            $q->bindValue($c, $kelas[$i], PDO::PARAM_STR);
            $a+=3; $b+=3; $c+=3;
        }
        $result = $q->execute();

        if($result) {
            $tgl = date("Y-m-d");
            $cnoInduk = count($noInduk);
        
            //get count values
            $rValue = ($cnoInduk  ? $cnoInduk - 1 : 0);
            $cValue = "(?,?,?,?,?)".str_repeat(",(?,?,?,?,?)", $rValue);
            
            $sql = "INSERT INTO transaksi_spp (no_induk,tanggal,dibayar,bangunan,lain_lain) VALUES $cValue";
            $q = $this->db->conn_id->prepare($sql);

            //function binding

            $a = 1; $b = 2; $c = 3; $d = 4; $e = 5;
            for($i=0; $i < $cnoInduk; $i++) {
                $q->bindValue($a, $noInduk[$i], PDO::PARAM_INT);
                $q->bindValue($b, $tgl, PDO::PARAM_STR);
                $q->bindValue($c, 0, PDO::PARAM_INT);
                $q->bindValue($d, 0, PDO::PARAM_INT);
                $q->bindValue($e, 0, PDO::PARAM_INT);
                $a+=5; $b+=5; $c+=5; $d+=5; $e+=5;
            }
            $result = $q->execute();
        }
    }

    public function check_siswa($noInduk) {
        $cnoInduk = count($noInduk);
        for($i = 0; $i < $cnoInduk; $i++) {
            $sql = "SELECT no_induk FROM siswa WHERE no_induk = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(':noInduk', $noInduk[$i], PDO::PARAM_INT);
            $q->execute();
            if($q->rowCount() > 0) {
                // if the key already exists then add it to the 
                // error array
                $noIndukerror[] = $noInduk[$i];
            }
        }
        if (!empty($noIndukerror)){
            return $noIndukerror;
        }

        return FALSE;
    }

    public function delete_siswa($noInduk) {
        $sql = "DELETE FROM siswa WHERE no_induk = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $delete = $q->execute();
        return $delete;
    }

    public function show_edit($noInduk) {
        $sql = "SELECT * FROM siswa WHERE no_induk = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetch(PDO::FETCH_ASSOC);
    }

    public function edit_siswa($noInduk, $nama, $kelas) {
        if($kelas == 'Lulus') {
            $sql = "UPDATE siswa SET keterangan = :kelas WHERE no_induk = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":kelas", $kelas, PDO::PARAM_STR);
            $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
            $q->execute();
        }
        $sql = "UPDATE siswa SET nama = :nama, kelas = :kelas, keterangan=null WHERE no_induk = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":nama", $nama, PDO::PARAM_STR);
        $q->bindParam(":kelas", $kelas, PDO::PARAM_STR);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        return $q->execute();
    }

    public function search_siswa($id, $key) {
        if($id == "int") {
            $sql = "SELECT * FROM siswa WHERE CAST(no_induk AS TEXT) LIKE ?";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_INT);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $sql = "SELECT * FROM siswa WHERE nama LIKE ?";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_STR);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function trash($noInduk) {
        // xlog($noInduk);
        $cnoInduk = count($noInduk);
        for ($i=0; $i < $cnoInduk ; $i++) { 
            $sql = "DELETE FROM siswa WHERE no_induk = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":noInduk", $noInduk[$i], PDO::PARAM_INT);
            $delete = $q->execute();
        }
        return $delete;
    }

    public function lulus($noInduk) {
        // xlog($noInduk);
        $cnoInduk = count($noInduk);
        for ($i=0; $i < $cnoInduk ; $i++) { 
            $sql = "UPDATE siswa SET kelas = 'Lulus', keterangan = 'Lulus' WHERE no_induk = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":noInduk", $noInduk[$i], PDO::PARAM_INT);
            $lulus = $q->execute();
        }

        if($lulus) {
            for ($i=0; $i < $cnoInduk ; $i++) { 
                $sql = " DELETE FROM transaksi_spp WHERE no_induk = :noInduk";
                $q = $this->db->conn_id->prepare($sql);
                $q->bindParam(":noInduk", $noInduk[$i], PDO::PARAM_INT);
                $lulus = $q->execute();
            }
        }

        return $lulus;
    }

    function password($data) {
        $sql = "UPDATE admin SET password = md5(:new_password) WHERE username = :username AND password = md5(:password)";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":new_password", $data['new_password']);
        $q->bindParam(":username", $data['username'], PDO::PARAM_STR);
        $q->bindParam(":password", $data['old_password']);
        return $q->execute();
    }
}

/* End of file m_database.php */
/* Location: ./application/models/m_database.php */