<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_reports extends CI_Model {
	function siswa() {
		$sql = "SELECT * FROM siswa";
	    $q = $this->db->conn_id->prepare($sql);
	    $q->execute();
	    return $result = $q->fetchAll(PDO::FETCH_ASSOC);
	    // xlog($result);
	    // exit();
	}

	function siswa_kelas($kelas, $nim){
		if(!empty($nim)) {
			$sql = "SELECT * FROM siswa WHERE no_induk::text LIKE ? AND kelas = ?";
			$q = $this->db->conn_id->prepare($sql);
			$q->bindValue(1, "%$nim%", PDO::PARAM_INT);
			$q->bindValue(2, $kelas, PDO::PARAM_STR);
			$q->execute();
			return $q->fetchAll(PDO::FETCH_ASSOC);
		} else {
			$sql = "SELECT * FROM siswa WHERE kelas = ?";
			$q = $this->db->conn_id->prepare($sql);
			$q->bindValue(1, $kelas, PDO::PARAM_STR);
			$q->execute();
			return $q->fetchAll(PDO::FETCH_ASSOC);
		}
	}

	function transaction() {
		$sql = "SELECT siswa.no_induk,siswa.nama,siswa.kelas,siswa.kelas,spp.spp,sum(transaksi_spp.dibayar)AS dibayar, 
		spp.spp - sum(transaksi_spp.dibayar)AS tunggakan FROM siswa,spp,transaksi_spp WHERE siswa.kelas = spp.kelas 
		AND siswa.no_induk = transaksi_spp.no_induk GROUP BY siswa.no_induk,spp.spp ORDER BY kelas, no_induk, nama";
		$q = $this->db->conn_id->prepare($sql);
		$q->execute();
		return $result = $q->fetchAll(PDO::FETCH_ASSOC);
		exit($result);
	}
}
/* End of file m_spp.php */
/* Location: ./application/models/m_reports.php */
