<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_guru extends CI_Model {
	public function count_guru() {
        $sql = "SELECT * FROM guru";
        $q = $this->db->conn_id->prepare($sql);
        $q->execute();
        return $q->rowCount();
    }

    public function fetch_guru($limit, $start) {
        $sql = "SELECT * FROM guru ORDER BY no_induk_guru LIMIT :limit OFFSET :start";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(':limit', $limit, PDO::PARAM_INT);
        $q->bindParam(':start', $start, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetchAll();
    }

    public function add_guru($data) {
        $cnoInduk = count($data['noInduk']);
        
        //get count values
        $rValue = ($cnoInduk  ? $cnoInduk - 1 : 0);
        $cValue = "(?,?,?,?,?,?,?,?,?)".str_repeat(",(?,?,?,?,?,?,?,?,?)", $rValue);
        
        $sql = "INSERT INTO guru (no_induk_guru, nama, ttl, jk, ijazah, jabatan, mengajar, bidang_studi, tlp) VALUES $cValue";
        $q = $this->db->conn_id->prepare($sql);
        //function binding
        $a = 1; $b = 2; $c = 3; $d = 4; $e = 5; $f = 6; $g = 7; $h = 8; $j = 9;
        for($i=0; $i < $cnoInduk; $i++) {
            $q->bindValue($a, $data['noInduk'][$i], PDO::PARAM_INT);
            $q->bindValue($b, $data['nama'][$i], PDO::PARAM_STR);
            $q->bindValue($c, $data['ttl'][$i], PDO::PARAM_STR);
            $q->bindValue($d, $data['jk'][$i], PDO::PARAM_STR);
            $q->bindValue($e, $data['ijazah'][$i], PDO::PARAM_STR);
            $q->bindValue($f, $data['jabatan'][$i], PDO::PARAM_STR);
            $q->bindValue($g, $data['mengajar'][$i], PDO::PARAM_INT);
            $q->bindValue($h, $data['bidang_studi'][$i], PDO::PARAM_STR);
            $q->bindValue($j, $data['tlp'][$i], PDO::PARAM_INT);
            $a+=9; $b+=9; $c+=9; $d+=9; $e+=9; $f+=9; $g+=9; $h+=9; $j+=9;
        }
        $q->execute();
    }

    public function check_guru($noInduk) {
        $cnoInduk = count($noInduk);
        for($i = 0; $i < $cnoInduk; $i++) {
            $sql = "SELECT no_induk_guru FROM guru WHERE no_induk_guru = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(':noInduk', $noInduk[$i], PDO::PARAM_INT);
            $q->execute();
            if($q->rowCount() > 0) {
                // if the key already exists then add it to the 
                // error array
                $noIndukerror[] = $noInduk[$i];
            }
        }
        if (!empty($noIndukerror)){
            return $noIndukerror;
        }
        return FALSE;
    }

    public function delete_guru($noInduk) {
        $sql = "DELETE FROM guru WHERE no_induk_guru = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $delete = $q->execute();
        return $delete;
    }

    public function show_edit($noInduk) {
        $sql = "SELECT * FROM guru WHERE no_induk_guru = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        $q->execute();

        return $result = $q->fetch(PDO::FETCH_ASSOC);
    }

    public function edit_guru($noInduk, $nama, $tlp, $alamat) {
        $sql = "UPDATE guru SET nama = :nama, tlp = :tlp, alamat = :alamat WHERE no_induk_guru = :noInduk";
        $q = $this->db->conn_id->prepare($sql);
        $q->bindParam(":nama", $nama, PDO::PARAM_STR);
        $q->bindParam(":tlp", $tlp, PDO::PARAM_INT);
        $q->bindParam(":alamat", $alamat, PDO::PARAM_STR);
        $q->bindParam(":noInduk", $noInduk, PDO::PARAM_INT);
        return $q->execute();
    }

    public function search_guru($id, $key) {
        if($id == "int") {
            $sql = "SELECT * FROM guru WHERE CAST(no_induk_guru AS TEXT) LIKE ?";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_INT);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $sql = "SELECT * FROM guru WHERE nama LIKE ?";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindValue(1, "%$key%", PDO::PARAM_STR);
            $q->execute();
            return $q->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function trash($noInduk) {
        // xlog($noInduk);
        $cnoInduk = count($noInduk);
        for ($i=0; $i < $cnoInduk ; $i++) { 
            $sql = "DELETE FROM guru WHERE no_induk_guru = :noInduk";
            $q = $this->db->conn_id->prepare($sql);
            $q->bindParam(":noInduk", $noInduk[$i], PDO::PARAM_INT);
            $delete = $q->execute();
        }
        return $delete;
    }
}
/* End of file m_guru.php */
/* Location: ./application/models/m_guru.php */